-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: laravel
-- ------------------------------------------------------
-- Server version	8.0.32-0ubuntu0.22.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Dr. Kieran Reinger','salvador67@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','bnrJztQPys','2023-03-20 15:17:55','2023-03-20 15:17:55'),(2,'Dudley Langosh','donato32@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','JsOTCkTSGJ','2023-03-20 15:17:55','2023-03-20 15:17:55'),(3,'Freida Turner','abbott.destany@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','C0le2tjToJ','2023-03-20 15:17:55','2023-03-20 15:17:55'),(4,'Prof. Jasen Olson','rkertzmann@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','V7q9L1YN2l','2023-03-20 15:17:55','2023-03-20 15:17:55'),(5,'Mrs. Yvonne Gusikowski IV','hermann.parker@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','UDoUnMeHNo','2023-03-20 15:17:55','2023-03-20 15:17:55'),(6,'Prof. Evans O\'Kon','geraldine.crist@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','kvo5xXMTLb','2023-03-20 15:17:55','2023-03-20 15:17:55'),(7,'Allen Stanton IV','fadel.araceli@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','ECpJZLnuA0','2023-03-20 15:17:56','2023-03-20 15:17:56'),(8,'Reggie Schumm','dwight.wilkinson@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','08agzMtw4r','2023-03-20 15:17:56','2023-03-20 15:17:56'),(9,'Prof. Jeremy Wyman Sr.','bernardo64@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','1Bzk2xhWMR','2023-03-20 15:17:56','2023-03-20 15:17:56'),(10,'Ellen Lang','baumbach.german@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','673y69YKC2','2023-03-20 15:17:56','2023-03-20 15:17:56'),(11,'Flo Towne III','chowe@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','razYObnvlE','2023-03-20 15:17:56','2023-03-20 15:17:56'),(12,'Gerhard Nitzsche','brayan84@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','8KJMsovswX','2023-03-20 15:17:57','2023-03-20 15:17:57'),(13,'Miss Carolina Monahan Sr.','cecilia69@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','4ih4CzDthC','2023-03-20 15:17:57','2023-03-20 15:17:57'),(14,'Rudy Dickens','raynor.alec@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','Bv7sZD7sAf','2023-03-20 15:17:57','2023-03-20 15:17:57'),(15,'Paolo Torphy','hildegard.spencer@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','DnbTmnYetV','2023-03-20 15:17:57','2023-03-20 15:17:57'),(16,'Joelle Bayer','jocelyn.torphy@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','4mqO46dwJZ','2023-03-20 15:17:57','2023-03-20 15:17:57'),(17,'Prof. Isaiah Conroy III','ularkin@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','DNv8BBp0ct','2023-03-20 15:17:57','2023-03-20 15:17:57'),(18,'Jermaine Mayert','cruickshank.julien@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','Sh2UuY7Tlp','2023-03-20 15:17:57','2023-03-20 15:17:57'),(19,'Breanna Keeling DVM','candido.wiza@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','wFKhusjr0b','2023-03-20 15:17:57','2023-03-20 15:17:57'),(20,'Jalon Cormier','kuvalis.zula@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','q9Z4KhfAjV','2023-03-20 15:17:57','2023-03-20 15:17:57'),(21,'Charley Stracke','bahringer.lenna@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','XOIHKFvCbE','2023-03-20 15:17:57','2023-03-20 15:17:57'),(22,'Ms. Sister Funk','crystel.bednar@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','VqrrvpnGri','2023-03-20 15:17:57','2023-03-20 15:17:57'),(23,'Hassan O\'Hara','sammie83@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','TSJtuaj1SM','2023-03-20 15:17:58','2023-03-20 15:17:58'),(24,'Shane Auer','stark.river@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','YSZZzc0TS8','2023-03-20 15:17:58','2023-03-20 15:17:58'),(25,'Aisha Nicolas','zlubowitz@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','R7MoCADXEZ','2023-03-20 15:17:58','2023-03-20 15:17:58'),(26,'Dell Waelchi','macie34@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','j469WbJsEf','2023-03-20 15:17:58','2023-03-20 15:17:58'),(27,'Muhammad Rice','raheem.bashirian@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','UxHxSdqaHe','2023-03-20 15:17:58','2023-03-20 15:17:58'),(28,'Mariane Block V','rahul13@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','8ZUhB6GnJX','2023-03-20 15:17:58','2023-03-20 15:17:58'),(29,'Prof. Christy Jacobson','huel.jamaal@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','bHbL1iI37c','2023-03-20 15:17:58','2023-03-20 15:17:58'),(30,'Nyasia Hermiston Jr.','domingo.eichmann@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','hPTjZScDe1','2023-03-20 15:17:58','2023-03-20 15:17:58'),(31,'Hollie Schroeder I','xgraham@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','ONcRSUIrac','2023-03-20 15:17:58','2023-03-20 15:17:58'),(32,'Gladys Hane','schuppe.nickolas@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','i7WlOGZUg1','2023-03-20 15:17:58','2023-03-20 15:17:58'),(33,'Dr. Edgardo Feest Jr.','dion57@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','3daSYGSewW','2023-03-20 15:17:59','2023-03-20 15:17:59'),(34,'Mallory Block','pspinka@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','MpWLuxN06K','2023-03-20 15:17:59','2023-03-20 15:17:59'),(35,'Katelin Turcotte','kelsie.carroll@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','WdU1mtTUcv','2023-03-20 15:17:59','2023-03-20 15:17:59'),(36,'Melba Leannon','aiden52@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','v0UBlfIwjC','2023-03-20 15:17:59','2023-03-20 15:17:59'),(37,'Prof. Yadira Barton','emmy48@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','Z3saW8cVvE','2023-03-20 15:17:59','2023-03-20 15:17:59'),(38,'Paolo Reichert','sschneider@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','1D405GQShU','2023-03-20 15:17:59','2023-03-20 15:17:59'),(39,'Mr. Wendell Mohr DVM','brittany32@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','J19JlOMkEg','2023-03-20 15:17:59','2023-03-20 15:17:59'),(40,'Dr. Sydnie King','karson90@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','z1w3ZzQ4Qs','2023-03-20 15:17:59','2023-03-20 15:17:59'),(41,'Mr. David Johnston','sabryna.hammes@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','6sQl4hgdEk','2023-03-20 15:17:59','2023-03-20 15:17:59'),(42,'Alek Brekke II','samara60@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','d82gxJHh6o','2023-03-20 15:18:00','2023-03-20 15:18:00'),(43,'Camila Denesik','armand.osinski@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','0d0dtutIxb','2023-03-20 15:18:00','2023-03-20 15:18:00'),(44,'Tiana Wilderman','wsteuber@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','cagm7Sm6VK','2023-03-20 15:18:00','2023-03-20 15:18:00'),(45,'Chelsea Reilly MD','oda.russel@example.net','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','vkyHubWi07','2023-03-20 15:18:00','2023-03-20 15:18:00'),(46,'Mrs. Retta Denesik DVM','keebler.obie@example.org','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','PEurXDKAg7','2023-03-20 15:18:00','2023-03-20 15:18:00'),(47,'Dr. Kamren O\'Kon DVM','liana.dare@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','ZZSx6cf68U','2023-03-20 15:18:00','2023-03-20 15:18:00'),(48,'Dr. Etha Littel II','tia.koelpin@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','vTXlO2lqcF','2023-03-20 15:18:01','2023-03-20 15:18:01'),(49,'Brook Reilly Jr.','kaitlyn.goodwin@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','BBTGl7M6yN','2023-03-20 15:18:01','2023-03-20 15:18:01'),(50,'Holly Herzog Sr.','nolan.dangelo@example.com','2023-03-20 15:17:55','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','hM84Gzn6x1','2023-03-20 15:18:01','2023-03-20 15:18:01');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-21  5:20:17
