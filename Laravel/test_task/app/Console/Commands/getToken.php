<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class getToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:gettoken {login} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for generating a token from a email and password';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('login'))->first();

    if (! $user || ! Hash::check($this->argument('password'), $user->password)) {
        $this->error('incorrect email or password');
        return '';
    }

    return $user->createToken($user->name)->plainTextToken;
    }
}
