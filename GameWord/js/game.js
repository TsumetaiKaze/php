function XmlHttp() {
	var xmlhttp	= false;
	try {
		xmlhttp	= new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e) {
			try {
				xmlhttp	= new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e) {
					xmlhttp	= false;
					}
				}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
		}
	return xmlhttp;
	}

 function addWordsList(player,inWord) {
	 if (inWord.trim()=='') return;
	 var wordsList	= document.getElementById("words");
	 var wordsList_value	= wordsList.value;
	 if (wordsList_value.trim()=='') wordsList_value	= player+': '+inWord;
		else wordsList_value	= wordsList_value+'\n'+player+': '+inWord;
	 wordsList.value	= wordsList_value;
	 }

function ajax(send)	{
	if (window.XMLHttpRequest) {
		var req = XmlHttp();
		req.open("POST","php/word.php",true);
		req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		req.send(send);
		req.onreadystatechange = function() {
			if (req.readyState == 4 && req.status == 200) {
				var resp	= req.responseText;
				if (resp=='000') {
					document.getElementById("win").innerHTML	= "YOU WIN!";
					document.getElementById("btn_clear").removeAttribute('hidden');
					}
					else if (resp=='!!!') {
						document.getElementById("win").innerHTML	= "YOU LOSE!";
						document.getElementById("btn_clear").removeAttribute('hidden');
						}
						else {
							addWordsList('Он',resp);
							}
				}
			}
		}
	}
 
 function OnEnter() {
	 var btn_clear	= document.getElementById("btn_clear");
	 if (!btn_clear.hasAttribute('hidden')) return;
	 var word	= document.getElementById("word").value;
	 addWordsList('Вы',word);
	 var send	= 'word='+word;
	 ajax(send);
	 }

 function OnClear() {
	 document.getElementById("win").innerHTML	= '';
	 document.getElementById("btn_clear").setAttribute('hidden', 'true');
	 document.getElementById("word").value	= '';
	 document.getElementById("words").value	= '';
	 }

 
 function funOnload() {
	 var btn_enter = document.getElementById("btn_enter");
	 btn_enter.onclick	= OnEnter;
	 var btn_clear = document.getElementById("btn_clear");
	 btn_clear.onclick	= OnClear;
	 }

 window.onload = funOnload;
