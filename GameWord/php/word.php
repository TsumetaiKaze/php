<?php
function conversionInVar($inVar) {
	$input_text	= trim($inVar);
	$input_text	= strip_tags($input_text);
	$input_text	= htmlspecialchars($input_text);
	while (strpos($input_text," ")!=false) {
		$input_text = str_replace(" ", "_", $input_text);
		}
	return $input_text;
	}

function sqlConn() {
	$mysqli	= new mysqli('127.0.0.1', 'u9991267_test', 'Admin2@19', 'u9991267_test');
	if (mysqli_connect_errno()) {
		$mysqli	= false;
		}
	return $mysqli;
	}
	
function getWord($lastLetter,$word) {
	$mysqli	= sqlConn();
	if ($mysqli==false) return false;
	$lastLetter	= $mysqli->real_escape_string($lastLetter);
	$word	= false;
	if ($stmt = $mysqli->prepare("SELECT word FROM Words WHERE letter=? AND word<>?")) {
		echo $stmt->error;
		$stmt->bind_param("ss", trim($lastLetter),trim($word));
		echo $stmt->error;
		$stmt->execute();
		echo $stmt->error;
		$stmt->bind_result($word);
		$stmt->fetch();
		$stmt->close();
		}
	$mysqli->close();
	return $word;
	}

function addWord($word,$letter) {
	$mysqli	= sqlConn();
	if ($mysqli==false) return false;
	$word	= $mysqli->real_escape_string($word);
	if ($stmt = $mysqli->prepare("INSERT INTO Words (word,letter) VALUES (?,?)")) {
		echo $stmt->error;
		$stmt->bind_param("ss", trim($word),trim($letter));
		echo $stmt->error;
		$stmt->execute();
		echo $stmt->error;
		$stmt->close();
		}
	$mysqli->close();
	}
	
$word	= conversionInVar($_POST['word']);

if (trim($word)=='') {
	echo '!!!';
	exit;
	}

$lastLetter	= mb_substr($word,-1);
if ($lastLetter=='ь') {
	$lastLetter	= mb_substr($word,-2,1);
	}

$wordResponse	= getWord($lastLetter,$word);
if ($wordResponse==false) {
	addWord($word,$lastLetter);
	echo '000';
	}
	else echo $wordResponse;
?>
