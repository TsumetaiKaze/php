function getId(inTxt) {
    let outTxT = inTxt.replace(/ /g,"-");
    outTxT = outTxT.replace(/\(/g,"");
    outTxT = outTxT.replace(/\)/g,"");
    return outTxT;
    }

function LandOnClick() {
    let Area = $('#'+getId(this.innerText));
    Area.slideToggle(300);
    }

function setCity(inCity) {
    let CityList = $('#CityList');
    let li = $('<li>'+inCity['name']+'</li>');
    li.on( "click" , LandOnClick);
    CityList.append(li);
    let ul = $('<ul id="'+getId(inCity['name'])+'">');
    CityList.append(ul);
    inCity['areas'].forEach(function(item, i, arr) {
        let li = $('<li>'+item['name']+'</li>');
        li.on( "click" , LandOnClick);
        ul.append(li);
        let ul1 = $('<ul id="'+getId(item['name'])+'">');
        ul.append(ul1);
        item['areas'].forEach(function(item1, i1, arr1) {
            let li = $('<li>'+item1['name']+'</li>');
            ul1.append(li);
            });
        li.click();
        });
    li.click();
    }

function getCity() {
    $.ajax({
        url: "php/city.php",
        success: function(data){
            let city = JSON.parse(data);
            city.forEach(function(item, i, arr) {
                setCity(item);
                });
            }
        });
    }

$(function() {
    getCity();
    });
