function setProf(inProf) {
    if (inProf['prof']===undefined) {
        var newElems = $('<option value='+inProf['id']+'>&nbsp;&nbsp;&nbsp;'+inProf['name']+'</option>');
        } else {
            var newElems = $('<option class="Group" value='+inProf['id']+'>'+inProf['name']+'</option>');
            }
    $('#selProf').append(newElems);
    if (inProf['prof']!==undefined) {
        inProf['prof'].forEach(function(item, i, arr) {
            setProf(item);
            });
        }
    }

function getProf() {
    $.ajax({
        url: "php/prof.php",
        success: function(data){
            let prof = JSON.parse(data);
            prof.forEach(function(item, i, arr) {
                setProf(item);
                });
            }
        });
    }

$(function() {
    getProf();
    });
