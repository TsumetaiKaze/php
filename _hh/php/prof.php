<?php
$specializations = file_get_contents('https://api.hh.ru/specializations');
if ($specializations===FALSE) {
    echo "error get specializations";
    exit;
    }
$spec = json_decode($specializations, true);
if ($spec===NULL) {
    echo "error convert json specializations";
    exit;
    }
$prof = array();
foreach ($spec as $group) {
    $profGr = array();
    $profGr['id']   = $group['id'];
    $profGr['name'] = $group['name'];
    $profs = array();
    foreach ($group['specializations'] as $profEl) {
        $prof1 = array();
        $prof1['id']   = $profEl['id'];
        $prof1['name'] = $profEl['name'];
        $profs[] = $prof1;
        }
    $profGr['prof'] = $profs;
    $prof[] = $profGr;
    }
echo json_encode($prof);
?>
