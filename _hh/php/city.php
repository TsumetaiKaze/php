<?php
$areas = file_get_contents('https://api.hh.ru/areas');
if ($areas===FALSE) {
    echo "error get areas";
    exit;
    }
$areasM = json_decode($areas, true);
if ($areasM===NULL) {
    echo "error convert json areas";
    exit;
    }
$citis = array();
foreach ($areasM as $lnd) {
    $land = array();
    $land['id']   = $lnd['id'];
    $land['name'] = $lnd['name'];
    $area = array();
    foreach ($lnd['areas'] as $ar) {
        $area1 = array();
        $area1['id']   = $ar['id'];
        $area1['name'] = $ar['name'];
        $city = array();
        foreach ($ar['areas'] as $ci) {
            $city1 = array();
            $city1['id']   = $ci['id'];
            $city1['name'] = $ci['name'];
            $city[] = $city1;
            }
        $area1['areas'] = $city;
        $area[] = $area1;
        }
    $land['areas'] = $area;
    $citis[] = $land;
    }
echo json_encode($citis);
?>
