async function ClickTxT() {
    let recid = this.getAttribute('recid');
    console.log(recid);
    let params = new URLSearchParams();
    params.set('recid', recid);
    let response = await fetch('/php/getDataTxT.php', {
        method: 'POST',
        body: params
        });
    let txt_box = document.getElementById('rigth');
    txt_box.innerHTML = await response.text();
    }

function ButtonClickShowHideChild() {
    let recid = this.getAttribute('recid');
    if (this.getAttribute('show')=='1') {
        this.setAttribute("value", "Развернуть");
        this.setAttribute("show",0);
        let ul = document.getElementById('ul_'+recid);
        ul.remove();
        }
        else {
            this.setAttribute("value", "Свернуть");
            this.setAttribute("show", 1);
            getData(recid);
            }
    }

function createButton(item,li) {
    if (item.attach) {
        let button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "Развернуть");
        button.setAttribute("recid", item.id);
        button.setAttribute("show", 0);
        button.onclick = ButtonClickShowHideChild;
        li.append(button);
        }
    }

function createTitle(result) {
    if (!Array.isArray(result)) return null;
    let ul = document.createElement('ul');
    let parent_id = 0;
    result.forEach((item)=> {
        let li = document.createElement('li');
        li.setAttribute("id", "li_"+item.id);
        li.setAttribute("recid", item.id);
        li.innerHTML = item.title;
        li.onclick = ClickTxT;
        ul.appendChild(li);
        createButton(item,li);
        parent_id = item.parent_id;
        });
    ul.setAttribute("id", "ul_"+parent_id);
    if (parent_id==0) {
        var parent_box = document.getElementById('left');
        }
        else {
            var parent_box = document.getElementById('li_'+parent_id);
            }
    parent_box.appendChild(ul);
    }

async function getData(select_id=0) {
    let params = new URLSearchParams();
    params.set('select_id', select_id);
    let response = await fetch('/php/getData.php', {
        method: 'POST',
        body: params
        });
    let result = await response.json();
    createTitle(result);
    }

function ready() {
    getData();
    }

document.addEventListener("DOMContentLoaded", ready);
