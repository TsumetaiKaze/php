<?php
class DB
{
    private $host    = '127.0.0.1';
    private $db      = 'test';
    private $user    = 'root';
    private $pass    = 'admin';
    private $charset = 'utf8';
    private $typeDB  = 'mysql';
    private $dsn     = '';
    private $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    private $pdo = null;
    private $err = '';
    
    public function connect() {
        $this->dsn = $this->typeDB.':host='.$this->host.';dbname='.$this->db.';charset='.$this->charset;
        try {
            $this->pdo = new PDO($this->dsn, $this->user, $this->pass, $this->opt);
            } catch (PDOException $e) {
                $this->pdo = null;
                $this->err = $this->err.'['.$e->getMessage().']';
                }        
    }
    
    public function close() {
        $this->pdo = null;
        }
    
   function __destruct() {
       $this->close();
   }
   
   private function checkField($row,$field,&$strData,$def='') {
       if (array_key_exists($field, $row)) {
           $strData[$field] = $row[$field];
           }
           else {
               $this->err = $this->err.'[field '.$field.' not exist]';
               $strData[$field] = $def;
               }
       }
   
   private function getStrDataSelectObject($row) {
       if (!is_array($row)) {
           $this->err = $this->err.'[incorrect str select db]';
           return null;
           }
       $strData = array();
       $this->checkField($row,'id',$strData,0);
       if ($strData['id']==0) {
           $this->err = $this->err.'[incorrect row id]';
           return null;
           }
       $this->checkField($row,'title',$strData,'');
       $this->checkField($row,'parent_id',$strData,0);
       $this->checkField($row,'attach',$strData,false);
       return $strData;
       }
   
   public function getObject($parent_id=0) {
       $result = array();
       if (!is_int($parent_id)) {
           $this->err = $this->err.'[incorrect parent_id]';
           return $result;
           }
       if ($this->pdo==null) {
           $this->err = $this->err.'[no connect db]';
           return $result;
           }
       $stmt = $this->pdo->prepare('SELECT id, title, parent_id, attach FROM test.articles WHERE parent_id =:parent_id;');
       $stmt->execute(array('parent_id' => $parent_id));
       foreach ($stmt as $row) {
           $strData = $this->getStrDataSelectObject($row);
           if (is_array($strData)) $result[] = $strData;
           }
       return $result;
       }

   private function getStrDataSelectTxT($row) {
       if (!is_array($row)) {
           $this->err = $this->err.'[incorrect str select db]';
           return '';
           }
       if (array_key_exists('txt', $row)) {
           $strData = $row['txt'];
           }
           else {
               $this->err = $this->err.'[field txt not exist]';
               $strData = '';
               }           
       return $strData;
       }

   public function getTxT($recid) {
       $result = '';
       if (!is_int($recid)) {
           $this->err = $this->err.'[incorrect recid]';
           return $result;
           }
       if ($this->pdo==null) {
           $this->err = $this->err.'[no connect db]';
           return $result;
           }
       $stmt = $this->pdo->prepare('SELECT txt FROM test.articles WHERE id =:recid;');
       $stmt->execute(array('recid' => $recid));
       $row = $stmt->fetch();
       $result = $this->getStrDataSelectTxT($row);
       return $result;
       }
}
?>
