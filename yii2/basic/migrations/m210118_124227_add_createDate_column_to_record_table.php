<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%record}}`.
 */
class m210118_124227_add_createDate_column_to_record_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%record}}', 'createDate', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%record}}', 'createDate');
    }
}
