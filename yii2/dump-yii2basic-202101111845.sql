PGDMP         -                 y         	   yii2basic #   12.5 (Ubuntu 12.5-0ubuntu0.20.04.1) #   12.5 (Ubuntu 12.5-0ubuntu0.20.04.1)     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16403 	   yii2basic    DATABASE     {   CREATE DATABASE yii2basic WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'ru_RU.UTF-8' LC_CTYPE = 'ru_RU.UTF-8';
    DROP DATABASE yii2basic;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1259    16404    country    TABLE     �   CREATE TABLE public.country (
    code character varying(2) NOT NULL,
    name character varying(52) NOT NULL,
    population integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.country;
       public         heap    postgres    false    3            �            1259    16410 	   migration    TABLE     g   CREATE TABLE public.migration (
    version character varying(180) NOT NULL,
    apply_time integer
);
    DROP TABLE public.migration;
       public         heap    postgres    false    3            �            1259    16417    user    TABLE     �  CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    auth_key character varying(32) NOT NULL,
    password_hash character varying(255) NOT NULL,
    password_reset_token character varying(255),
    email character varying(255) NOT NULL,
    status smallint DEFAULT 10 NOT NULL,
    created_at integer NOT NULL,
    updated_at integer NOT NULL
);
    DROP TABLE public."user";
       public         heap    postgres    false    3            �            1259    16415    user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.user_id_seq;
       public          postgres    false    3    205            �           0    0    user_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;
          public          postgres    false    204                       2604    16420    user id    DEFAULT     d   ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);
 8   ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            �          0    16404    country 
   TABLE DATA           9   COPY public.country (code, name, population) FROM stdin;
    public          postgres    false    202            �          0    16410 	   migration 
   TABLE DATA           8   COPY public.migration (version, apply_time) FROM stdin;
    public          postgres    false    203            �          0    16417    user 
   TABLE DATA           �   COPY public."user" (id, username, auth_key, password_hash, password_reset_token, email, status, created_at, updated_at) FROM stdin;
    public          postgres    false    205            �           0    0    user_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.user_id_seq', 2, true);
          public          postgres    false    204                       2606    16408    country country_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pk PRIMARY KEY (code);
 <   ALTER TABLE ONLY public.country DROP CONSTRAINT country_pk;
       public            postgres    false    202            !           2606    16414    migration migration_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);
 B   ALTER TABLE ONLY public.migration DROP CONSTRAINT migration_pkey;
       public            postgres    false    203            #           2606    16432    user user_email_key 
   CONSTRAINT     Q   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);
 ?   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_email_key;
       public            postgres    false    205            %           2606    16430 "   user user_password_reset_token_key 
   CONSTRAINT     o   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_password_reset_token_key UNIQUE (password_reset_token);
 N   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_password_reset_token_key;
       public            postgres    false    205            '           2606    16426    user user_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
       public            postgres    false    205            )           2606    16428    user user_username_key 
   CONSTRAINT     W   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);
 B   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_username_key;
       public            postgres    false    205            �   �   x�5��
�0Dד��$��M�e[��ҝ��-�m]��k�g3Su���<�-
�j�֪;�Kq��q�y�P��7�[ԣ����e�Th$I/(8��٨������謭���]���2۴،���eS�%������.���:�l�إ���P�V�Z��s�2��M��i��� bk6�      �   I   x��5 �x(��X��ihfh`ljdafʕkdh`hho`ahdl�\��X�_Z�Z_���Skl`h����� 80T      �   �   x�U�MO�0 ��s�;WJ���	�h���%s
��Y�_oc2o��}HO�h�L��sa�c��=��RX���1��l�e��f)�h�2!ߐ�m՜��-�H�=�U�w�=�k>Aۦ���1��w�]��b�`?�D��	��uj�a��*�2��k�$~��!�S2����T�G��r�M{�n����
&�b>�z��
?hu��b����U\H��vI�Y,�[W����i?�2W�     