<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CModule::IncludeModule('iblock');
global $APPLICATION;
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");

if(isset($_GET['PAGE_I'])){
  $page = $_GET['PAGE_I'];
}else{
  $page = 1;
}

$redirect = false;

$iblockId = 27; // ИД инфоблока

$resElement = CIBlockElement::GetList(
		array ('ID' => 'DESC'),
		array (
			//'ACTIVE' => 'Y',
			'IBLOCK_ID' 		=> $iblockId,
			'CATALOG_QUANTITY'	=> 0		/// здесь должно быть условие фильтрации
		),
		false,
   		array ("nTopCount" => 50), //
		array ('IBLOCK_ID', 'ID', 'XML_ID', 'NAME','CATALOG_QUANTITY')
	);

	while($arElement = $resElement->Fetch()){
		$redirect = true;
		$arRes[] = $arElement['ID'];
		AddMessage2Log("ID: ".$arElement['ID']." XML_ID: ".$arElement['XML_ID']." NAME: ".$arElement['NAME'], "arElement");
		$boolResult = CIBlockElement::Delete($arElement['ID']);
		if ($boolResult) {
			AddMessage2Log("delete ID: ".$arElement['ID']." XML_ID: ".$arElement['XML_ID']." NAME: ".$arElement['NAME'], "arElement");
			}
	}

if($redirect){
  $url = $APPLICATION->GetCurUri();
  if(strpos($url, 'PAGE_I='.$page) === FALSE){
    $url = $APPLICATION->GetCurUri('PAGE_I='.($page+1));
  }else{
    $url = str_replace('PAGE_I='.$page, 'PAGE_I='.($page+1), $url);
  }

	LocalRedirect('http://185.219.40.14'.$url);
}else{
  var_dump('Y');
}
