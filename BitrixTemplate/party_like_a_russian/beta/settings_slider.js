$(document).on('ready', function(){
    var slider0 = $(".slick-carousel0");	
    var slider1 = $(".slick-carousel1");
    var slider2 = $(".slick-carousel2");
    var slider3 = $(".slick-carousel3");
    var slider4 = $(".slick-carousel4");
    var slider5 = $(".slick-carousel5");
    var slider6 = $(".slick-carousel6");
    
   slider1.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,     
        dots:true, 
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'        
      }); 

   slider2.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });       
    
   slider3.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      }); 
      
   slider4.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });
      
   slider5.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1, 
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });  
      
   slider6.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });   

   slider0.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      }); 
   
   
    function blink_my(object){
       object.css("opacity", 0);
       setTimeout(function(){object.css("opacity", 1);},500); 
    }
   
    //swipe
    $('#block_6_0').on('swipe', function(event, slick, direction){
       blink_my($("#block_6_0 .desc_auto_mobile"));
    });
    
    $('#block_6_1').on('swipe', function(event, slick, direction){       
       blink_my($("#block_6_1 .desc_auto_mobile"));
    });
    
    $('#block_6_2').on('swipe', function(event, slick, direction){       
       blink_my($("#block_6_2 .desc_auto_mobile"));
    });

    $('#block_6_3').on('swipe', function(event, slick, direction){       
       blink_my($("#block_6_3 .desc_auto_mobile"));
    });

    $('#block_6_4').on('swipe', function(event, slick, direction){       
       blink_my($("#block_6_4 .desc_auto_mobile"));
    });
    
    $('#block_6_5').on('swipe', function(event, slick, direction){       
       blink_my($("#block_6_5 .desc_auto_mobile"));
    });
  
  
  
    //next click
    $('#block_6_0 .next_nav').on('click', function(event, slick, direction){
       blink_my($("#block_6_0 .desc_auto_mobile"));
    });
    
    $('#block_6_1 .next_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_1 .desc_auto_mobile"));
    });
    
    $('#block_6_2 .next_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_2 .desc_auto_mobile"));
    });

    $('#block_6_3 .next_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_3 .desc_auto_mobile"));
    });

    $('#block_6_4 .next_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_4 .desc_auto_mobile"));
    });
    
    $('#block_6_5 .next_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_5 .desc_auto_mobile"));
    });
    
    //prev click
    $('#block_6_0 .prev_nav').on('click', function(event, slick, direction){
       blink_my($("#block_6_0 .desc_auto_mobile"));
    });
    
    $('#block_6_1 .prev_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_1 .desc_auto_mobile"));
    });
    
    $('#block_6_2 .prev_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_2 .desc_auto_mobile"));
    });

    $('#block_6_3 .prev_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_3 .desc_auto_mobile"));
    });

    $('#block_6_4 .prev_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_4 .desc_auto_mobile"));
    });
    
    $('#block_6_5 .prev_nav').on('click', function(event, slick, direction){       
       blink_my($("#block_6_5 .desc_auto_mobile"));
    });
    
});