$(document).on('ready', function(){
    var slider0 = $(".slick-carousel0");	
    var slider1 = $(".slick-carousel1");
    var slider2 = $(".slick-carousel2");
    var slider3 = $(".slick-carousel3");
    var slider4 = $(".slick-carousel4");
    var slider5 = $(".slick-carousel5");
    var slider6 = $(".slick-carousel6");
    
   slider1.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,     
        dots:true, 
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'        
      }); 

   slider2.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });       
    
   slider3.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      }); 
      
   slider4.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });
      
   slider5.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1, 
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });  
      
   slider6.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      });   

   slider0.slick({
        arrows: true,
        fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,                
        dots:true,
        prevArrow: '<div class="prev_nav"><</div>',
        nextArrow: '<div class="next_nav">></div>'           
      }); 
   
});