<!DOCTYPE html>
<html xml:lang='ru' lang='ru' xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<title> - Хризолит</title>
	<meta name="cmsmagazine" content="836973d6bc23e1a0d1cb5de4f56acfee" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width" />
	<meta name="HandheldFriendly" content="true" />
	<meta name="yes" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/bitrix/js/main/core/css/core.min.css?15877179162854" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/core/css/core_popup.min.css?158771791618079" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/core/css/core_date.min.css?15877179169657" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/fileman/sticker.min.css?158771791324594" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/core/css/core_panel.min.css?158771791662290" type="text/css"  rel="stylesheet" />
<link href="/local/templates/hrizolit/components/bitrix/sale.basket.basket.line/normal/style.min.css?15877179133326" type="text/css"  rel="stylesheet" />
<link href="/bitrix/wizards/aspro/mshop/css/panel.css?1587717963241" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/css/jquery.fancybox.min.css?15877179113213" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/css/styles.min.css?1587717911110112" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/ajax/ajax.min.css?1587717913247" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/panel/main/popup.min.css?158771791320704" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/panel/main/admin-public.min.css?158771791372199" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/panel/main/hot_keys.min.css?15877179133150" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/themes/.default/pubstyles.min.css?158771791746822" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/styles.min.css?15877179122876" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/template_styles.css?1587717912280313" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/themes/custom_s1/theme.min.css?158771791128883" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/css/media.min.css?158771791181090" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/hrizolit/css/custom.css?158771791116690" type="text/css"  data-template-style="true"  rel="stylesheet" />
<script type="text/javascript">if(!window.BX)window.BX={};if(!window.BX.message)window.BX.message=function(mess){if(typeof mess=='object') for(var i in mess) BX.message[i]=mess[i]; return true;};</script>
<script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_WINDOW_CONTINUE':'Продолжить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'PULL_OLD_REVISION':'Для продолжения корректной работы с сайтом необходимо перезагрузить страницу.'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'WEEK_START':'1'});(window.BX||top.BX).message({'MONTH_1':'Январь','MONTH_2':'Февраль','MONTH_3':'Март','MONTH_4':'Апрель','MONTH_5':'Май','MONTH_6':'Июнь','MONTH_7':'Июль','MONTH_8':'Август','MONTH_9':'Сентябрь','MONTH_10':'Октябрь','MONTH_11':'Ноябрь','MONTH_12':'Декабрь','MONTH_1_S':'Января','MONTH_2_S':'Февраля','MONTH_3_S':'Марта','MONTH_4_S':'Апреля','MONTH_5_S':'Мая','MONTH_6_S':'Июня','MONTH_7_S':'Июля','MONTH_8_S':'Августа','MONTH_9_S':'Сентября','MONTH_10_S':'Октября','MONTH_11_S':'Ноября','MONTH_12_S':'Декабря','MON_1':'Янв','MON_2':'Фев','MON_3':'Мар','MON_4':'Апр','MON_5':'Май','MON_6':'Июн','MON_7':'Июл','MON_8':'Авг','MON_9':'Сен','MON_10':'Окт','MON_11':'Ноя','MON_12':'Дек','DAY_OF_WEEK_0':'Воскресенье','DAY_OF_WEEK_1':'Понедельник','DAY_OF_WEEK_2':'Вторник','DAY_OF_WEEK_3':'Среда','DAY_OF_WEEK_4':'Четверг','DAY_OF_WEEK_5':'Пятница','DAY_OF_WEEK_6':'Суббота','DOW_0':'Вс','DOW_1':'Пн','DOW_2':'Вт','DOW_3':'Ср','DOW_4':'Чт','DOW_5':'Пт','DOW_6':'Сб','FD_SECOND_AGO_0':'#VALUE# секунд назад','FD_SECOND_AGO_1':'#VALUE# секунду назад','FD_SECOND_AGO_10_20':'#VALUE# секунд назад','FD_SECOND_AGO_MOD_1':'#VALUE# секунду назад','FD_SECOND_AGO_MOD_2_4':'#VALUE# секунды назад','FD_SECOND_AGO_MOD_OTHER':'#VALUE# секунд назад','FD_SECOND_DIFF_0':'#VALUE# секунд','FD_SECOND_DIFF_1':'#VALUE# секунда','FD_SECOND_DIFF_10_20':'#VALUE# секунд','FD_SECOND_DIFF_MOD_1':'#VALUE# секунда','FD_SECOND_DIFF_MOD_2_4':'#VALUE# секунды','FD_SECOND_DIFF_MOD_OTHER':'#VALUE# секунд','FD_MINUTE_AGO_0':'#VALUE# минут назад','FD_MINUTE_AGO_1':'#VALUE# минуту назад','FD_MINUTE_AGO_10_20':'#VALUE# минут назад','FD_MINUTE_AGO_MOD_1':'#VALUE# минуту назад','FD_MINUTE_AGO_MOD_2_4':'#VALUE# минуты назад','FD_MINUTE_AGO_MOD_OTHER':'#VALUE# минут назад','FD_MINUTE_DIFF_0':'#VALUE# минут','FD_MINUTE_DIFF_1':'#VALUE# минута','FD_MINUTE_DIFF_10_20':'#VALUE# минут','FD_MINUTE_DIFF_MOD_1':'#VALUE# минута','FD_MINUTE_DIFF_MOD_2_4':'#VALUE# минуты','FD_MINUTE_DIFF_MOD_OTHER':'#VALUE# минут','FD_MINUTE_0':'#VALUE# минут','FD_MINUTE_1':'#VALUE# минуту','FD_MINUTE_10_20':'#VALUE# минут','FD_MINUTE_MOD_1':'#VALUE# минуту','FD_MINUTE_MOD_2_4':'#VALUE# минуты','FD_MINUTE_MOD_OTHER':'#VALUE# минут','FD_HOUR_AGO_0':'#VALUE# часов назад','FD_HOUR_AGO_1':'#VALUE# час назад','FD_HOUR_AGO_10_20':'#VALUE# часов назад','FD_HOUR_AGO_MOD_1':'#VALUE# час назад','FD_HOUR_AGO_MOD_2_4':'#VALUE# часа назад','FD_HOUR_AGO_MOD_OTHER':'#VALUE# часов назад','FD_HOUR_DIFF_0':'#VALUE# часов','FD_HOUR_DIFF_1':'#VALUE# час','FD_HOUR_DIFF_10_20':'#VALUE# часов','FD_HOUR_DIFF_MOD_1':'#VALUE# час','FD_HOUR_DIFF_MOD_2_4':'#VALUE# часа','FD_HOUR_DIFF_MOD_OTHER':'#VALUE# часов','FD_YESTERDAY':'вчера','FD_TODAY':'сегодня','FD_TOMORROW':'завтра','FD_DAY_AGO_0':'#VALUE# суток назад','FD_DAY_AGO_1':'#VALUE# сутки назад','FD_DAY_AGO_10_20':'#VALUE# суток назад','FD_DAY_AGO_MOD_1':'#VALUE# сутки назад','FD_DAY_AGO_MOD_2_4':'#VALUE# суток назад','FD_DAY_AGO_MOD_OTHER':'#VALUE# суток назад','FD_DAY_DIFF_0':'#VALUE# дней','FD_DAY_DIFF_1':'#VALUE# день','FD_DAY_DIFF_10_20':'#VALUE# дней','FD_DAY_DIFF_MOD_1':'#VALUE# день','FD_DAY_DIFF_MOD_2_4':'#VALUE# дня','FD_DAY_DIFF_MOD_OTHER':'#VALUE# дней','FD_DAY_AT_TIME':'#DAY# в #TIME#','FD_MONTH_AGO_0':'#VALUE# месяцев назад','FD_MONTH_AGO_1':'#VALUE# месяц назад','FD_MONTH_AGO_10_20':'#VALUE# месяцев назад','FD_MONTH_AGO_MOD_1':'#VALUE# месяц назад','FD_MONTH_AGO_MOD_2_4':'#VALUE# месяца назад','FD_MONTH_AGO_MOD_OTHER':'#VALUE# месяцев назад','FD_MONTH_DIFF_0':'#VALUE# месяцев','FD_MONTH_DIFF_1':'#VALUE# месяц','FD_MONTH_DIFF_10_20':'#VALUE# месяцев','FD_MONTH_DIFF_MOD_1':'#VALUE# месяц','FD_MONTH_DIFF_MOD_2_4':'#VALUE# месяца','FD_MONTH_DIFF_MOD_OTHER':'#VALUE# месяцев','FD_YEARS_AGO_0':'#VALUE# лет назад','FD_YEARS_AGO_1':'#VALUE# год назад','FD_YEARS_AGO_10_20':'#VALUE# лет назад','FD_YEARS_AGO_MOD_1':'#VALUE# год назад','FD_YEARS_AGO_MOD_2_4':'#VALUE# года назад','FD_YEARS_AGO_MOD_OTHER':'#VALUE# лет назад','FD_YEARS_DIFF_0':'#VALUE# лет','FD_YEARS_DIFF_1':'#VALUE# год','FD_YEARS_DIFF_10_20':'#VALUE# лет','FD_YEARS_DIFF_MOD_1':'#VALUE# год','FD_YEARS_DIFF_MOD_2_4':'#VALUE# года','FD_YEARS_DIFF_MOD_OTHER':'#VALUE# лет','CAL_BUTTON':'Выбрать','CAL_TIME_SET':'Установить время','CAL_TIME':'Время'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'ADMIN_INCLAREA_DBLCLICK':'Двойной щелчок','ADMIN_SHOW_MODE_ON':'включен','ADMIN_SHOW_MODE_OFF':'выключен','AMDIN_SHOW_MODE_TITLE':'Режим правки','ADMIN_SHOW_MODE_ON_HINT':'Режим правки включен. На странице доступны элементы управления компонентами и включаемыми областями.','ADMIN_SHOW_MODE_OFF_HINT':'Режим правки выключен'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'25200','SITE_ID':'s1','SITE_DIR':'/','USER_ID':'1','SERVER_TIME':'1588095271','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'6985fd67f6bbfdf811e1f40e7a010051'});</script>


<script type="text/javascript" src="/bitrix/cache/js/s1/hrizolit/kernel_main/kernel_main.js?1587723707280214"></script>
<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js?158771791593636"></script>
<script type="text/javascript" src="/bitrix/js/main/ajax.min.js?158771791522194"></script>
<script type="text/javascript" src="/bitrix/cache/js/s1/hrizolit/kernel_currency/kernel_currency.js?15877199071821"></script>
<script type="text/javascript" src="/bitrix/cache/js/s1/hrizolit/kernel_pull/kernel_pull.js?158772370731171"></script>
<script type="text/javascript" src="/bitrix/js/fileman/sticker.min.js?158771791348559"></script>
<script type="text/javascript" src="/bitrix/js/main/core/core_admin.min.js?158771791621881"></script>
<script type="text/javascript" src="/bitrix/js/main/admin_tools.js?158771791667477"></script>
<script type="text/javascript" src="/bitrix/js/main/popup_menu.min.js?15877179169738"></script>
<script type="text/javascript" src="/bitrix/js/main/admin_search.min.js?15877179164820"></script>
<script type="text/javascript" src="/bitrix/js/main/hot_keys.min.js?158771791612333"></script>
<script type="text/javascript" src="/bitrix/js/main/public_tools.min.js?158771791624131"></script>
<script type="text/javascript">BX.setJSList(['/bitrix/js/main/core/core.js','/bitrix/js/main/core/core_ajax.js','/bitrix/js/main/json/json2.min.js','/bitrix/js/main/core/core_ls.js','/bitrix/js/main/session.js','/bitrix/js/main/core/core_window.js','/bitrix/js/main/core/core_popup.js','/bitrix/js/main/core/core_date.js','/bitrix/js/main/utils.js','/bitrix/js/currency/core_currency.js','/bitrix/js/pull/pull.js','/local/templates/hrizolit/js/jquery.actual.min.js','/local/templates/hrizolit/js/jqModal.js','/local/templates/hrizolit/js/jquery.fancybox.js','/local/templates/hrizolit/js/jquery.history.js','/local/templates/hrizolit/js/jquery.flexslider.js','/local/templates/hrizolit/js/jquery.validate.min.js','/local/templates/hrizolit/js/jquery.inputmask.bundle.min.js','/local/templates/hrizolit/js/jquery.easing.1.3.js','/local/templates/hrizolit/js/equalize.min.js','/local/templates/hrizolit/js/jquery.alphanumeric.js','/local/templates/hrizolit/js/jquery.cookie.js','/local/templates/hrizolit/js/jquery.plugin.min.js','/local/templates/hrizolit/js/jquery.countdown.min.js','/local/templates/hrizolit/js/jquery.countdown-ru.js','/local/templates/hrizolit/js/jquery.ikSelect.js','/local/templates/hrizolit/js/sly.js','/local/templates/hrizolit/js/equalize_ext.js','/local/templates/hrizolit/js/main.js','/bitrix/components/bitrix/search.title/script.js','/local/templates/hrizolit/js/custom.js','/local/templates/hrizolit/components/bitrix/sale.basket.basket.line/normal/script.js']); </script>
<script type="text/javascript">var bxDate = new Date(); document.cookie="BITRIX_SM_TIME_ZONE="+bxDate.getTimezoneOffset()+"; path=/; expires=Fri, 01-Jan-2038 00:00:00 GMT"</script>
<script type="text/javascript">
					(function () {
						"use strict";

						var counter = function ()
						{
							var cookie = (function (name) {
								var parts = ("; " + document.cookie).split("; " + name + "=");
								if (parts.length == 2) {
									try {return JSON.parse(decodeURIComponent(parts.pop().split(";").shift()));}
									catch (e) {}
								}
							})("BITRIX_CONVERSION_CONTEXT_s1");

							if (cookie && cookie.EXPIRE >= BX.message("SERVER_TIME") && cookie.UNIQUE && cookie.UNIQUE.length > 0)
							{
								for (var i = 0; i < cookie.UNIQUE.length; i++)
								{
									if (cookie.UNIQUE[i] == "conversion_visit_day")
										return;
								}
							}

							var request = new XMLHttpRequest();
							request.open("POST", "/bitrix/tools/conversion/ajax_counter.php", true);
							request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							request.send(
								"SITE_ID="+encodeURIComponent("s1")+
								"&sessid="+encodeURIComponent(BX.bitrix_sessid())+
								"&HTTP_REFERER="+encodeURIComponent(document.referrer)
							);
						};

						if (window.frameRequestStart === true)
							BX.addCustomEvent("onFrameDataReceived", counter);
						else
							BX.ready(counter);
					})();
				</script>
<script>BX.message({'PHONE':'Телефон','SOCIAL':'Социальные сети','DESCRIPTION':'Описание магазина','ITEMS':'Товары','LOGO':'Логотип','REGISTER_INCLUDE_AREA':'Текст о регистрации','AUTH_INCLUDE_AREA':'Текст об авторизации','FRONT_IMG':'Изображение компании','EMPTY_CART':'пуста','CATALOG_VIEW_MORE':'... Показать все','CATALOG_VIEW_LESS':'... Свернуть','JS_REQUIRED':'Заполните это поле!','JS_FORMAT':'Неверный формат!','JS_FILE_EXT':'Недопустимое расширение файла!','JS_PASSWORD_COPY':'Пароли не совпадают!','JS_PASSWORD_LENGTH':'Минимум 6 символов!','JS_ERROR':'Неверно заполнено поле!','JS_FILE_SIZE':'Максимальный размер 5мб!','JS_FILE_BUTTON_NAME':'Выберите файл','JS_FILE_DEFAULT':'Файл не найден','JS_DATE':'Некорректная дата!','JS_REQUIRED_LICENSES':'\"Согласие с условиями\" обязательно для заполнения','FANCY_CLOSE':'Закрыть','FANCY_NEXT':'Следующий','FANCY_PREV':'Предыдущий','TOP_AUTH_REGISTER':'Регистрация','CALLBACK':'Заказать звонок','UNTIL_AKC':'До конца акции','TITLE_QUANTITY_BLOCK':'Остаток','TITLE_QUANTITY':'штук','COUNTDOWN_SEC':'сек.','COUNTDOWN_MIN':'мин.','COUNTDOWN_HOUR':'час.','COUNTDOWN_DAY0':'дней','COUNTDOWN_DAY1':'день','COUNTDOWN_DAY2':'дня','COUNTDOWN_WEAK0':'Недель','COUNTDOWN_WEAK1':'Неделя','COUNTDOWN_WEAK2':'Недели','COUNTDOWN_MONTH0':'Месяцев','COUNTDOWN_MONTH1':'Месяц','COUNTDOWN_MONTH2':'Месяца','COUNTDOWN_YEAR0':'Лет','COUNTDOWN_YEAR1':'Год','COUNTDOWN_YEAR2':'Года','CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR':'Заполнены не все свойства у добавляемого товара','CATALOG_EMPTY_BASKET_PROPERTIES_ERROR':'Выберите свойства товара, добавляемые в корзину в параметрах компонента','CATALOG_ELEMENT_NOT_FOUND':'Элемент не найден','ERROR_ADD2BASKET':'Ошибка добавления товара в корзину','CATALOG_SUCCESSFUL_ADD_TO_BASKET':'Успешное добавление товара в корзину','ERROR_BASKET_TITLE':'Ошибка корзины','ERROR_BASKET_PROP_TITLE':'Выберите свойства, добавляемые в корзину','ERROR_BASKET_BUTTON':'Выбрать','BASKET_TOP':'Корзина в шапке','ERROR_ADD_DELAY_ITEM':'Ошибка отложенной корзины','VIEWED_TITLE':'Ранее вы смотрели','VIEWED_BEFORE':'Ранее вы смотрели','BEST_TITLE':'Лучшие предложения','FROM':'от','TITLE_BLOCK_VIEWED_NAME':'Ранее вы смотрели','BASKET_CHANGE_TITLE':'Ваш заказ','BASKET_CHANGE_LINK':'Изменить','FULL_ORDER':'Оформление обычного заказа','BASKET_PRINT_BUTTON':'Распечатать заказ','BASKET_CLEAR_ALL_BUTTON':'Очистить','BASKET_QUICK_ORDER_BUTTON':'Быстрый заказ','BASKET_CONTINUE_BUTTON':'Продолжить покупки','BASKET_ORDER_BUTTON':'Оформить заказ','FRONT_STORES':'Заголовок со списком элементов'})</script>
<link rel="shortcut icon" href="/favicon.ico?1587717992" type="image/x-icon" />
<link rel="apple-touch-icon" sizes="57x57" href="/include/favicon_57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="/include/favicon_72.png" />
<meta property="og:title" content=" - Хризолит" />
<meta property="og:type" content="article" />
<meta property="og:image" content="/logo.png" />
<link rel="image_src" href="/logo.png"  />
<meta property="og:url" content="/local/php_interface/uploadToCSV.php?clear_cache=Y" />

<script type="text/javascript">
var phpVars = {
	'ADMIN_THEME_ID': '.default',
	'LANGUAGE_ID': 'ru',
	'FORMAT_DATE': 'DD.MM.YYYY',
	'FORMAT_DATETIME': 'DD.MM.YYYY HH:MI:SS',
	'opt_context_ctrl': false,
	'cookiePrefix': 'BITRIX_SM',
	'titlePrefix': 'Хризолит - ',
	'bitrix_sessid': '6985fd67f6bbfdf811e1f40e7a010051',
	'messHideMenu': 'Скрыть меню',
	'messShowMenu': 'Показать меню',
	'messHideButtons': 'Уменьшить кнопки',
	'messShowButtons': 'Увеличить кнопки',
	'messFilterInactive': 'Поиск не используется - показаны все записи',
	'messFilterActive': 'Используется поиск - показаны только найденные записи',
	'messFilterLess': 'Скрыть условие поиска',
	'messLoading': 'Загрузка...',
	'messMenuLoading': 'Загрузка...',
	'messMenuLoadingTitle': 'Загружаются пункты меню...',
	'messNoData': '- Нет данных -',
	'messExpandTabs': 'Развернуть все вкладки на одну страницу',
	'messCollapseTabs': 'Свернуть вкладки',
	'messPanelFixOn': 'Зафиксировать панель на экране',
	'messPanelFixOff': 'Открепить панель',
	'messPanelCollapse': 'Скрыть панель',
	'messPanelExpand': 'Показать панель'
};
</script>




<script type="text/javascript" src="/bitrix/cache/js/s1/hrizolit/template_a2d98b92d6c943aac13399adbf161bef/template_a2d98b92d6c943aac13399adbf161bef.js?1587719912299259"></script>
<script type="text/javascript" src="/bitrix/cache/js/s1/hrizolit/default_738ba88b26d81acfd4f2e9d5b34ad9c5/default_738ba88b26d81acfd4f2e9d5b34ad9c5.js?15877199074454"></script>
<script type="text/javascript">
bxSession.mess.messSessExpired = 'Ваш сеанс работы с сайтом завершен из-за отсутствия активности в течение 15 мин. Введенные на странице данные не будут сохранены. Скопируйте их перед тем, как закроете или обновите страницу.';
bxSession.Expand(900, '6985fd67f6bbfdf811e1f40e7a010051', true, '1371cf05f7f25b17c4a0c36adeaa14f9');
</script>
<script type="text/javascript">BX.bind(window, "load", function() { BX.PULL.start({'CHANNEL_ID':'693d666fc485868c02dbd4fd16f4d474','CHANNEL_DT':'1588061792','USER_ID':'1','LAST_ID':'0','PATH':'/bitrix/components/bitrix/pull.request/ajax.php?UPDATE_STATE','PATH_MOD':'','PATH_WS':'','PATH_COMMAND':'','METHOD':'PULL','REVISION':'14','ERROR':''}); });</script>
<script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "367e9367ca2a53f2d3b3a520838e1b58"]); _ba.push(["host", "127.0.0.1"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>
<script>new Image().src='http://185.219.40.14/bitrix/spread.php?s=QklUUklYX1NNX0FCVEVTVF9zMQEBMTYxOTE5OTI3MQEvAQEBAkJJVFJJWF9TTV9HVUVTVF9JRAEzMTE5MAExNjE5MTk5MjY4AS8BAQECQklUUklYX1NNX0xBU1RfVklTSVQBMjkuMDQuMjAyMCAwMDozNDoyOAExNjE5MTk5MjY4AS8BAQEC&k=89ecd9633f6b6c321e90a65e3f504214';
</script>


			<!--[if gte IE 9]><style type="text/css">.basket_button, .button30, .icon {filter: none;}</style><![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
	<body id="main">
		<div id="panel">
	<!--[if lte IE 7]>
	<style type="text/css">#bx-panel {display:none !important;}</style>
	<div id="bx-panel-error">Административная панель не поддерживает Internet Explorer версии 7 и ниже. Установите современный браузер <a href="http://www.firefox.com">Firefox</a>, <a href="http://www.google.com/chrome/">Chrome</a>, <a href="http://www.opera.com">Opera</a> или <a href="http://www.microsoft.com/windows/internet-explorer/">Internet Explorer 8</a>.</div><![endif]-->
	<script type="text/javascript">BX.admin.dynamic_mode=false; BX.admin.dynamic_mode_show_borders = false;</script>
	<div style="display:none; overflow:hidden;" id="bx-panel-back"></div>
	<div id="bx-panel" class="bx-panel-folded">
		<div id="bx-panel-top">
			<div id="bx-panel-top-gutter"></div>
			<div id="bx-panel-tabs">
	
				<a id="bx-panel-menu" href="" onmouseover="BX.hint(this, 'Меню', 'Быстрый переход на любую страницу Административной части.')"><span id="bx-panel-menu-icon"></span><span id="bx-panel-menu-text">Меню</span></a><a id="bx-panel-view-tab"><span>Сайт</span></a><a id="bx-panel-admin-tab" href="/bitrix/admin/index.php?lang=ru&amp;back_url_pub=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY"><span>Администрирование</span></a><script type="text/javascript">BX.message({MENU_ENABLE_TOOLTIP: true}); new BX.COpener({'DIV':'bx-panel-menu','ACTIVE_CLASS':'bx-pressed','MENU_URL':'/bitrix/admin/get_start_menu.php?lang=ru&back_url_pub=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&sessid=6985fd67f6bbfdf811e1f40e7a010051','MENU_PRELOAD':false});</script><script type="text/javascript"> BXHotKeys.Add("", "var d=BX(\"bx-panel-menu\"); if (d) d.click();", 91, 'Меню', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "var d=BX(\'bx-panel-admin-tab\'); if (d) location.href = d.href;", 92, 'Администрирование', 0); </script><a class="adm-header-notif-block" id="adm-header-notif-block" onclick="return BX.adminInformer.Toggle(this);" href="" title="Просмотр уведомлений"><span class="adm-header-notif-icon"></span><span id="adm-header-notif-counter" class="adm-header-notif-counter">5</span></a><a id="bx-panel-clear-cache" href="" onclick="BX.clearCache(); return false;"><span id="bx-panel-clear-cache-icon"></span><span id="bx-panel-clear-cache-text">Сбросить кеш</span></a>
			</div>
			<div id="bx-panel-userinfo">
	<a href="/bitrix/admin/user_edit.php?lang=ru&ID=1" id="bx-panel-user" onmouseover="BX.hint(this, 'Быстрый переход на страницу редактирования параметров пользователя.')"><span id="bx-panel-user-icon"></span><span id="bx-panel-user-text">Юка</span></a><a href="/local/php_interface/uploadToCSV.php?logout=yes&amp;clear_cache=Y" id="bx-panel-logout" onmouseover="BX.hint(this, 'Выход пользователя из системы. (&amp;nbsp;Ctrl+Alt+O&amp;nbsp;) ')">Выйти</a><a href="/local/php_interface/uploadToCSV.php?bitrix_include_areas=Y&amp;clear_cache=Y" id="bx-panel-toggle" class="bx-panel-toggle-off" onmouseover="BX.hint(this, 'Режим правки', 'Включение режима контекстного редактирования сайта. При включенном режиме наведите курсор на выбранный блок информации и используйте всплывающие панели инструментов. (&amp;nbsp;Ctrl+Alt+D&amp;nbsp;) ')"><span id="bx-panel-switcher-gutter-left"></span><span id="bx-panel-toggle-indicator"><span id="bx-panel-toggle-icon"></span><span id="bx-panel-toggle-icon-overlay"></span></span><span class="bx-panel-break"></span><span id="bx-panel-toggle-caption">Режим правки</span><span class="bx-panel-break"></span><span id="bx-panel-toggle-caption-mode"><span id="bx-panel-toggle-caption-mode-off">выключен</span><span id="bx-panel-toggle-caption-mode-on">включен</span></span><span id="bx-panel-switcher-gutter-right"></span></a><a href="" id="bx-panel-expander" onmouseover="BX.hint(this, 'Развернуть', 'Развернуть Панель управления. (&amp;nbsp;Ctrl+Alt+E&amp;nbsp;) ')"><span id="bx-panel-expander-text">Развернуть</span><span id="bx-panel-expander-arrow"></span></a><a id="bx-panel-hotkeys" href="javascript:void(0)" onclick="BXHotKeys.ShowSettings();" onmouseover="BX.hint(this, 'Настройки горячих клавиш (&amp;nbsp;Ctrl+Alt+P&amp;nbsp;) ')"></a><a href="javascript:void(0)" id="bx-panel-pin" onmouseover="BX.hint(this, 'Фиксация Панели управления в верхней части экрана.')"></a><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+79", "var d=BX(\'bx-panel-logout\'); if (d) location.href = d.href;", 133, 'Выход пользователя из системы.', 19); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+68", "location.href=\"/local/php_interface/uploadToCSV.php?bitrix_include_areas=Y&clear_cache=Y\";", 117, 'Режим правки', 16); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+69", "var d=BX(\'bx-panel-expander\'); if (d) BX.fireEvent(d, \'click\');", 118, 'Развернуть/Свернуть', 21); </script>
			</div>
		</div>
	<div id="bx-panel-site-toolbar"><div id="bx-panel-buttons-gutter"></div><div id="bx-panel-switcher"><a href="" id="bx-panel-hider" onmouseover="BX.hint(this, 'Свернуть', 'Свернуть Панель управления. (&amp;nbsp;Ctrl+Alt+E&amp;nbsp;) ')">Свернуть<span id="bx-panel-hider-arrow"></span></a></div><div id="bx-panel-buttons"><div id="bx-panel-buttons-inner"><span class="bx-panel-button-group" data-group-id="0"><script type="text/javascript"> BXHotKeys.Add("", "", 111, '<b>Создать страницу</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=hrizolit&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 60, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Создать страницу', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/workflow_edit.php?lang=ru&site=s1&fname=/local/php_interface/untitled.php&template=standart.php&return_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')", 108, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Через документооборот', 0); </script><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_file_new.php?lang=ru&amp;site=s1&amp;templateID=hrizolit&amp;path=%2Flocal%2Fphp_interface%2F&amp;back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&amp;siteTemplateId=hrizolit','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_create"><span class="bx-panel-button-icon bx-panel-create-page-icon"></span></a><a id="bx_topmenu_btn_create_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Создать<span class="bx-panel-break"></span>страницу&nbsp;<span class="bx-panel-button-arrow"></span></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Создать страницу','TEXT':'Создание страниц с помощью мастера.'}, GROUP_ID : 0, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_menu', TYPE: 'BIG', MENU: [{'TEXT':'Создать страницу','TITLE':'Мастер создания новой страницы','ICON':'panel-new-file','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=hrizolit&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','DEFAULT':true,'SORT':'10','HK_ID':'top_panel_create_page'},{'SEPARATOR':true,'SORT':'49'},{'SRC':'/bitrix/images/workflow/new_page.gif','TEXT':'Через документооборот','TITLE':'Создать новую страницу в текущем разделе через документооборот','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/workflow_edit.php?lang=ru&site=s1&fname=/local/php_interface/untitled.php&template=standart.php&return_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')','HK_ID':'FLOW_PANEL_CREATE_WITH_WF','SORT':'50'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Создать новую страницу','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_file_edit.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F&new=Y&templateID=hrizolit&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Создать страницу','TEXT':'Нажмите на эту кнопку, чтобы запустить мастер или шаблон создания страниц или включаемых областей для страниц. С помощью этих мастеров, можно создать страницу или включаемую область, последовательно внося данные на каждом шаге мастера.'}, GROUP_ID : 0, TEXT :  "Создать страницу"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 112, '<b>Создать раздел</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=hrizolit&newFolder=Y&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 62, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Создать раздел', 0); </script><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_file_new.php?lang=ru&amp;site=s1&amp;templateID=hrizolit&amp;newFolder=Y&amp;path=%2Flocal%2Fphp_interface%2F&amp;back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&amp;siteTemplateId=hrizolit','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_create_section"><span class="bx-panel-button-icon bx-panel-create-section-icon"></span></a><a id="bx_topmenu_btn_create_section_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Создать<span class="bx-panel-break"></span>раздел&nbsp;<span class="bx-panel-button-arrow"></span></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_section', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Создать раздел','TEXT':'Создание разделов с помощью мастера.'}, GROUP_ID : 0, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_section_menu', TYPE: 'BIG', MENU: [{'TEXT':'Создать раздел','TITLE':'Мастер создания нового раздела','ICON':'panel-new-folder','DEFAULT':true,'ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=hrizolit&newFolder=Y&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'10','HK_ID':'top_panel_create_folder'},{'TEXT':'По шаблону','TITLE':'Мастер создания нового раздела с индексной страницей по шаблону','ICON':'panel-new-folder-template','MENU':[{'TEXT':'Форум (ЧПУ)','TITLE':'Шаблон forum\nСтраница с форумами в режиме ЧПУ','ICON':'','IMAGE':'/bitrix/themes/.default/start_menu/forum/forum.gif','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&wiz_template=forum&lang=ru&site=s1&templateID=hrizolit&newFolder=Y&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'},{'SEPARATOR':true},{'TEXT':'Стандартная страница','TITLE':'Шаблон standard.php','ICON':'panel-new-file-template','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&page_template=standard.php&lang=ru&site=s1&templateID=hrizolit&newFolder=Y&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'}],'SORT':'20'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Создать новый раздел','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_newfolder.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Создать раздел','TEXT':'Нажмите на эту кнопку, чтобы запустить мастер или шаблон создания разделов или включаемых областей для разделов. С помощью этих мастеров, можно создать страницу или включаемую область, последовательно внося данные на каждом шаге мастера.'}, GROUP_ID : 0, TEXT :  "Создать раздел"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 107, '<b>Изменить страницу</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&templateID=hrizolit&siteTemplateId=hrizolit\',\'width\':\'1086\',\'height\':\'698\',\'min_width\':\'780\',\'min_height\':\'400\'})).Show()", 63, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В визуальном редакторе', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_property.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 64, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Заголовок и свойства страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&noeditor=Y&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'1086\',\'height\':\'698\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 65, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В режиме HTML-кода', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&templateID=hrizolit&siteTemplateId=hrizolit\',\'width\':\'1132\',\'height\':\'400\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 67, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В режиме PHP-кода', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_delete.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&siteTemplateId=hrizolit\',\'width\':\'440\',\'height\':\'180\',\'min_width\':\'250\',\'min_height\':\'180\'})).Show()", 68, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Удалить страницу', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/workflow_edit.php?lang=ru&site=s1&fname=/local/php_interface/uploadToCSV.php&return_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')", 109, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Через документооборот', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/workflow_history_list.php?lang=ru&find_filename=%2Flocal%2Fphp_interface%2FuploadToCSV.php&find_filename_exact_match=Y&set_filter=Y\')", 110, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;История изменений страницы', 0); </script></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="1"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CEditorDialog({'content_url':'/bitrix/admin/public_file_edit.php?lang=ru&amp;path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&amp;site=s1&amp;back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&amp;templateID=hrizolit&amp;siteTemplateId=hrizolit','width':'1086','height':'698','min_width':'780','min_height':'400'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_edit"><span class="bx-panel-button-icon bx-panel-edit-page-icon"></span></a><a id="bx_topmenu_btn_edit_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Изменить<span class="bx-panel-break"></span>страницу&nbsp;<span class="bx-panel-button-arrow"></span></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Изменить страницу','TEXT':'Изменение содержимого страницы.'}, GROUP_ID : 1, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_menu', TYPE: 'BIG', MENU: [{'TEXT':'В визуальном редакторе','TITLE':'Редактировать страницу в визуальном редакторе','ICON':'panel-edit-visual','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&templateID=hrizolit&siteTemplateId=hrizolit\',\'width\':\'1086\',\'height\':\'698\',\'min_width\':\'780\',\'min_height\':\'400\'})).Show()','DEFAULT':true,'SORT':'10','HK_ID':'top_panel_edit_page'},{'TEXT':'Заголовок и свойства страницы','TITLE':'Изменить заголовок и другие свойства страницы','ICON':'panel-file-props','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_property.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'20','HK_ID':'top_panel_page_prop'},{'SEPARATOR':true,'SORT':'29'},{'TEXT':'Доступ к странице','TITLE':'Установить права доступа к странице','ICON':'panel-file-access','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'30','HK_ID':'top_panel_access_page_new'},{'ID':'delete','ICON':'icon-delete','ALT':'Удалить страницу','TEXT':'Удалить страницу','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_delete.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&siteTemplateId=hrizolit\',\'width\':\'440\',\'height\':\'180\',\'min_width\':\'250\',\'min_height\':\'180\'})).Show()','SORT':'40','HK_ID':'top_panel_del_page'},{'SEPARATOR':true,'SORT':'49'},{'TEXT':'В режиме HTML-кода','TITLE':'Редактировать страницу в режиме HTML','ICON':'panel-edit-text','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&noeditor=Y&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'1086\',\'height\':\'698\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()','SORT':'50','HK_ID':'top_panel_edit_page_html'},{'TEXT':'В режиме PHP-кода','TITLE':'Редактировать полный PHP-код страницы','ICON':'panel-edit-php','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&templateID=hrizolit&siteTemplateId=hrizolit\',\'width\':\'1132\',\'height\':\'400\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()','SORT':'60','HK_ID':'top_panel_edit_page_php'},{'SEPARATOR':true,'SORT':'79'},{'SRC':'/bitrix/images/workflow/edit_flow_public.gif','TEXT':'Через документооборот','TITLE':'Редактирование текущей страницы через документооборот','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/workflow_edit.php?lang=ru&site=s1&fname=/local/php_interface/uploadToCSV.php&return_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')','HK_ID':'FLOW_PANEL_EDIT_WITH_WF','SORT':'80'},{'SRC':'/bitrix/images/workflow/history.gif','TEXT':'История изменений страницы','TITLE':'История изменений текущей страницы','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/workflow_history_list.php?lang=ru&find_filename=%2Flocal%2Fphp_interface%2FuploadToCSV.php&find_filename_exact_match=Y&set_filter=Y\')','HK_ID':'FLOW_PANEL_HISTORY','SORT':'81'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Редактировать текущую страницу','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_file_edit.php?lang=ru&site=s1&templateID=hrizolit&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Изменить страницу','TEXT':'Нажмите на эту кнопку, чтобы вызвать диалог редактирования страницы, в том числе через документооборот, просмотреть историю изменений страницы, изменить свойства страницы, ограничить доступ к ней.'}, GROUP_ID : 1, TEXT :  "Изменить страницу"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 96, '<b>Изменить раздел</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "javascript:(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_folder_edit.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 69, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Свойства раздела', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 70, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Доступ к разделу', 0); </script><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_folder_edit.php?lang=ru&amp;site=s1&amp;path=%2Flocal%2Fphp_interface%2F&amp;back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&amp;siteTemplateId=hrizolit','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_edit_section"><span class="bx-panel-button-icon bx-panel-edit-section-icon"></span></a><a id="bx_topmenu_btn_edit_section_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Изменить<span class="bx-panel-break"></span>раздел&nbsp;<span class="bx-panel-button-arrow"></span></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_section', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Изменить раздел','TEXT':'Изменение свойств раздела.'}, GROUP_ID : 1, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_section_menu', TYPE: 'BIG', MENU: [{'TEXT':'Свойства раздела','TITLE':'Изменить название и другие свойства раздела','ICON':'panel-folder-props','DEFAULT':true,'ACTION':'javascript:(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_folder_edit.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'10','HK_ID':'top_panel_folder_prop'},{'TEXT':'Доступ к разделу','TITLE':'Установить права доступа к разделу','ICON':'panel-folder-access','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'30','HK_ID':'top_panel_access_folder_new'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Редактировать свойства раздела','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_folder.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Изменить раздел','TEXT':'Нажмите на эту кнопку, чтобы изменить свойства раздела и ограничить доступ к нему.'}, GROUP_ID : 1, TEXT :  "Изменить раздел"})</script></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="2"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_menus"><span class="bx-panel-small-button-icon bx-panel-menu-icon"></span><span class="bx-panel-small-button-text">Меню</span><span class="bx-panel-small-single-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_menus', TYPE: 'SMALL', MENU: [{'TEXT':'Редактировать \"bottom_company\"','TITLE':'Редактировать пункты меню \"bottom_company\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2F&name=bottom_company&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"bottom_help\"','TITLE':'Редактировать пункты меню \"bottom_help\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2F&name=bottom_help&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"bottom_info\"','TITLE':'Редактировать пункты меню \"bottom_info\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2F&name=bottom_info&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"top_catalog\"','TITLE':'Редактировать пункты меню \"top_catalog\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2F&name=top_catalog&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"top_general\"','TITLE':'Редактировать пункты меню \"top_general\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2F&name=top_general&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"type_1\"','TITLE':'Редактировать пункты меню \"type_1\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2F&name=type_1&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"Нижнее меню\"','TITLE':'Редактировать пункты меню \"Нижнее меню\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2F&name=bottom&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'SEPARATOR':'Y','SORT':'150'},{'TEXT':'Создать \"bottom_company\"','TITLE':'Создать меню \"bottom_company\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2Flocal%2Fphp_interface&name=bottom_company&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"bottom_help\"','TITLE':'Создать меню \"bottom_help\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2Flocal%2Fphp_interface&name=bottom_help&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"bottom_info\"','TITLE':'Создать меню \"bottom_info\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2Flocal%2Fphp_interface&name=bottom_info&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"top_catalog\"','TITLE':'Создать меню \"top_catalog\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2Flocal%2Fphp_interface&name=top_catalog&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"top_general\"','TITLE':'Создать меню \"top_general\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2Flocal%2Fphp_interface&name=top_general&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"type_1\"','TITLE':'Создать меню \"type_1\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2Flocal%2Fphp_interface&name=type_1&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"Нижнее меню\"','TITLE':'Создать меню \"Нижнее меню\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&path=%2Flocal%2Fphp_interface&name=bottom&siteTemplateId=hrizolit\',\'width\':\'890\',\'height\':\'413\'})).Show()','DEFAULT':false}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Меню','TEXT':'Вызов формы редактирования меню. Нажмите на стрелку, чтобы отредактировать все меню данной страницы или создать новое меню.','TARGET':'parent'}, GROUP_ID : 2, TEXT :  "Меню"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 97, '<b>Структура</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/fileman_admin.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F\')", 71, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В панели управления', 0); </script><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_structure.php?lang=ru&amp;site=s1&amp;path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&amp;templateID=hrizolit&amp;siteTemplateId=hrizolit','width':'350','height':'470'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_structure"><span class="bx-panel-small-button-icon bx-panel-site-structure-icon"></span><span class="bx-panel-small-button-text">Структура</span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_structure', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Структура','TEXT':'Вызов диалога Структура сайта с возможностью добавления, редактирования, перемещения, удаления, изменения свойств разделов и страниц.','TARGET':'parent'}, GROUP_ID : 2, SKIP : true, LINK: "javascript:void(0)", ACTION : "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_structure.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&templateID=hrizolit&siteTemplateId=hrizolit\',\'width\':\'350\',\'height\':\'470\'})).Show()",TEXT :  "Структура" })</script><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_structure_menu"><span class="bx-panel-small-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_structure_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Управление структурой','TITLE':'Управление структурой сайта','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_structure.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&templateID=hrizolit&siteTemplateId=hrizolit\',\'width\':\'350\',\'height\':\'470\'})).Show()','DEFAULT':true,'HK_ID':'main_top_panel_struct'},{'SEPARATOR':true},{'TEXT':'В панели управления','TITLE':'Перейти в управление структурой в административном разделе','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_admin.php?lang=ru&site=s1&path=%2Flocal%2Fphp_interface%2F\')','HK_ID':'main_top_panel_struct_panel'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 2, TEXT :  "Структура"})</script></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CAdminDialog({'content_url':'/bitrix/admin/public_seo_tools.php?lang=ru&amp;bxpublic=Y&amp;from_module=seo&amp;site=s1&amp;path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&amp;title_final=&amp;title_changer_name=&amp;title_changer_link=&amp;title_win_final=IC0g0KXRgNC40LfQvtC70LjRgg%3D%3D&amp;title_win_changer_name=&amp;title_win_changer_link=&amp;sessid=6985fd67f6bbfdf811e1f40e7a010051&amp;back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&amp;siteTemplateId=hrizolit','width':'920','height':'400'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-active')" id="bx_topmenu_btn_seo"><span class="bx-panel-small-button-icon bx-panel-seo-icon"></span><span class="bx-panel-small-button-text">SEO</span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_seo', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'SEO','TEXT':'Вызов инструментов оптимизации сайта с целью повышения положения сайта в выдаче поисковых машин.'}, GROUP_ID : 2, SKIP : false, LINK: "javascript:void(0)", ACTION : "(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/public_seo_tools.php?lang=ru&bxpublic=Y&from_module=seo&site=s1&path=%2Flocal%2Fphp_interface%2FuploadToCSV.php&title_final=&title_changer_name=&title_changer_link=&title_win_final=IC0g0KXRgNC40LfQvtC70LjRgg%3D%3D&title_win_changer_name=&title_win_changer_link=&sessid=6985fd67f6bbfdf811e1f40e7a010051&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'920\',\'height\':\'400\'})).Show()",TEXT :  "SEO" })</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 98, '<b>Сбросить кеш</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "BX.clearCache()", 72, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Обновить кеш страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?clear_cache_session=Y&clear_cache=Y\');", 74, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Не использовать кеш', 0); </script></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="3"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="BX.clearCache();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_0"><span class="bx-panel-button-icon bx-panel-clear-cache-icon"></span></a><a id="bx_topmenu_btn_0_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Сбросить<span class="bx-panel-break"></span>кеш&nbsp;<span class="bx-panel-button-arrow"></span></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_0', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Сбросить кеш','TEXT':'Обновление кеша страницы.'}, GROUP_ID : 3, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_0_menu', TYPE: 'BIG', MENU: [{'TEXT':'Обновить кеш страницы','TITLE':'Обновить закешированные данные текущей страницы','ICON':'panel-page-cache','ACTION':'BX.clearCache()','DEFAULT':true,'HK_ID':'top_panel_cache_page'},{'SEPARATOR':true},{'TEXT':'Не использовать кеш','TITLE':'Отключить кеширование для текущего сеанса пользователя','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?clear_cache_session=Y&clear_cache=Y\');','HK_ID':'top_panel_cache_not'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Сбросить кеш','TEXT':'Нажмите на эту кнопку, чтобы обновить кеш страницы или компонентов, или отключить кеш для данной страницы вообще.'}, GROUP_ID : 3, TEXT :  "Сбросить кеш"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], BX(\'bx-panel-toggle\').href);", 75, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Перейти в режим правки', 0); </script></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="4"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_components"><span class="bx-panel-small-button-icon bx-panel-components-icon"></span><span class="bx-panel-small-button-text">Компоненты</span><span class="bx-panel-small-single-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_components', TYPE: 'SMALL', MENU: [{'TEXT':'Перейти в режим правки','TITLE':'Перейдите в режим правки для отображения списка компонентов','ACTION':'jsUtils.Redirect([], BX(\'bx-panel-toggle\').href);','HK_ID':'top_panel_edit_mode'}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Компоненты','TEXT':'Выбор компонента для редактирования его параметров.','TARGET':'parent'}, GROUP_ID : 4, TEXT :  "Компоненты"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 99, '<b>Шаблон сайта</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fhrizolit%2Fstyles.css&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'1132\',\'height\':\'400\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 76, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Изменить стили сайта', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fhrizolit%2Ftemplate_styles.css&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'1132\',\'height\':\'400\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 77, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Изменить стили шаблона', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/site_edit.php?lang=ru&LID=s1\')", 78, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Редактировать сайт', 0); </script><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_1"><span class="bx-panel-small-button-icon bx-panel-site-template-icon"></span><span class="bx-panel-small-button-text">Шаблон сайта</span><span class="bx-panel-small-single-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_1', TYPE: 'SMALL', MENU: [{'TEXT':'Изменить стили сайта','TITLE':'Редактировать основные стили сайта (styles.css)','ICON':'panel-edit-text','HK_ID':'top_panel_templ_site_css','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fhrizolit%2Fstyles.css&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'1132\',\'height\':\'400\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()'},{'TEXT':'Изменить стили шаблона','TITLE':'Редактировать стили шаблона (template_styles.css)','ICON':'panel-edit-text','HK_ID':'top_panel_templ_templ_css','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fhrizolit%2Ftemplate_styles.css&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'1132\',\'height\':\'400\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()'},{'SEPARATOR':'Y'},{'TEXT':'В панели управления','MENU':[{'TEXT':'Редактировать шаблон','TITLE':'Редактировать шаблон сайта в панели управления','ICON':'icon-edit','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/template_edit.php?lang=ru&ID=hrizolit\')','DEFAULT':false,'HK_ID':'top_panel_templ_edit'},{'TEXT':'Редактировать сайт','TITLE':'Редактировать настройки сайта в панели управления','ICON':'icon-edit','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/site_edit.php?lang=ru&LID=s1\')','DEFAULT':false,'HK_ID':'top_panel_templ_site'}]}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Шаблон сайта','TEXT':'Вызов форм редактирования стилей сайта и стилей шаблона.','TARGET':'parent'}, GROUP_ID : 4, TEXT :  "Шаблон сайта"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 100, '<b>Отладка</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_sql_stat=Y&clear_cache=Y\');", 83, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика SQL-запросов', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_include_exec_time=Y&clear_cache=Y\');", 82, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика включаемых областей', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_page_exec_time=Y&clear_cache=Y\');", 81, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Время исполнения страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?compress=Y&clear_cache=Y\');", 84, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика компрессии', 0); </script><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="/local/php_interface/uploadToCSV.php?show_page_exec_time=Y&amp;show_include_exec_time=Y&amp;show_sql_stat=Y&amp;clear_cache=Y" onclick=";BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_2"><span class="bx-panel-small-button-icon bx-panel-performance-icon"></span><span class="bx-panel-small-button-text">Отладка</span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_2', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Отладка','TEXT':'Включение режима отображения суммарной статистики. Нажмите на стрелку, чтобы Просмотреть статистику запросов базы данных, статистику включаемых областей, время исполнение страницы и статистику компрессии.','TARGET':'parent'}, GROUP_ID : 4, SKIP : true, LINK: "/local/php_interface/uploadToCSV.php?show_page_exec_time=Y&show_include_exec_time=Y&show_sql_stat=Y&clear_cache=Y", ACTION : "",TEXT :  "Отладка" })</script><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_2_menu"><span class="bx-panel-small-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_2_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Суммарная статистика','TITLE':'Показывать статистику SQL-запросов, включаемых областей, времени исполнения','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_page_exec_time=Y&show_include_exec_time=Y&show_sql_stat=Y&clear_cache=Y\');','DEFAULT':true,'HK_ID':'top_panel_debug_summ'},{'SEPARATOR':true},{'TEXT':'Статистика SQL-запросов','TITLE':'Показывать статистику SQL-запросов, исполняющихся на странице','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_sql_stat=Y&clear_cache=Y\');','HK_ID':'top_panel_debug_sql'},{'TEXT':'Детальная статистика кеша','TITLE':'Внимание! Может сильно влиять на время исполнения страницы.','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_cache_stat=Y&clear_cache=Y\');','HK_ID':'top_panel_debug_cache'},{'TEXT':'Статистика включаемых областей','TITLE':'Показывать статистику включаемых областей (время, запросы)','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_include_exec_time=Y&clear_cache=Y\');','HK_ID':'top_panel_debug_incl'},{'TEXT':'Время исполнения страницы','TITLE':'Показывать время исполнения страницы','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_page_exec_time=Y&clear_cache=Y\');','HK_ID':'top_panel_debug_time'},{'SEPARATOR':true},{'TEXT':'Статистика компрессии','TITLE':'Показывать статистику модуля компрессии','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?compress=Y&clear_cache=Y\');','HK_ID':'top_panel_debug_compr'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 4, TEXT :  "Отладка"})</script></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="5"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_3"><span class="bx-panel-small-button-icon bx-panel-statistics-icon"></span><span class="bx-panel-small-button-text">Статистика</span><span class="bx-panel-small-single-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_3', TYPE: 'SMALL', MENU: [{'TEXT':'График посещаемости страницы','TITLE':'График посещаемости страницы','IMAGE':'/bitrix/images/statistic/page_traffic.gif','ACTION':'javascript:window.open(\'/bitrix/admin/section_graph_list.php?lang=ru&public=Y&width=650&height=650&section=http%3A%2F%2F127.0.0.1%2Flocal%2Fphp_interface%2FuploadToCSV.php&set_default=Y\',\'\',\'target=_blank,scrollbars=yes,resizable=yes,width=650,height=650,left=\'+Math.floor((screen.width - 650)/2)+\',top=\'+Math.floor((screen.height- 650)/2))'},{'TEXT':'Показать статистику переходов по ссылкам с данной страницы','TITLE':'Показать статистику переходов по ссылкам с данной страницы','IMAGE':'/bitrix/images/statistic/link_stat_show.gif','ACTION':'jsUtils.Redirect([], \'/local/php_interface/uploadToCSV.php?show_link_stat=Y&clear_cache=Y\')'}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Статистика','TEXT':'Просмотр графика посещаемости страницы или статистики переходов.','TARGET':'parent'}, GROUP_ID : 5, TEXT :  "Статистика"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 101, '<b>Короткий URL</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "javascript:(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'770\',\'height\':\'270\'})).Show()", 85, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Добавить короткую ссылку', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/short_uri_admin.php?lang=ru\');", 86, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список ссылок', 0); </script><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CAdminDialog({'content_url':'/bitrix/admin/short_uri_edit.php?lang=ru&amp;public=Y&amp;bxpublic=Y&amp;str_URI=%2Flocal%2Fphp_interface%2FuploadToCSV.php&amp;site=s1&amp;back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&amp;siteTemplateId=hrizolit','width':'770','height':'270'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_4"><span class="bx-panel-small-button-icon bx-panel-short-url-icon"></span><span class="bx-panel-small-button-text">Короткий URL</span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_4', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Короткая ссылка','TEXT':'Настройка короткой ссылки для текущей страницы','TARGET':'parent'}, GROUP_ID : 5, SKIP : true, LINK: "javascript:void(0)", ACTION : "(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'770\',\'height\':\'270\'})).Show()",TEXT :  "Короткий URL" })</script><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_4_menu"><span class="bx-panel-small-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_4_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Добавить короткую ссылку','TITLE':'Добавить короткую ссылку на текущую страницу','ACTION':'javascript:(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Flocal%2Fphp_interface%2FuploadToCSV.php&site=s1&back_url=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY&siteTemplateId=hrizolit\',\'width\':\'770\',\'height\':\'270\'})).Show()','DEFAULT':true,'HK_ID':'MTP_SHORT_URI1'},{'TEXT':'Список ссылок','TITLE':'Список коротких ссылок','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/short_uri_admin.php?lang=ru\');','HK_ID':'MTP_SHORT_URI_LIST'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 5, TEXT :  "Короткий URL"})</script></span></span><script type="text/javascript"> BXHotKeys.Add("", "", 102, '<b>Стикеры</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+83", "if (window.oBXSticker){window.oBXSticker.AddSticker();}", 87, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Наклеить стикер', 22); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+88", "if (window.oBXSticker){window.oBXSticker.ShowAll();}", 88, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Показать стикеры (0)', 23); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+76", "if (window.oBXSticker){window.oBXSticker.ShowList(\'current\');}", 89, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список стикеров страницы', 24); </script><script type="text/javascript"> BXHotKeys.Add("", "if (window.oBXSticker){window.oBXSticker.ShowList(\'all\');}", 90, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список всех стикеров', 0); </script><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="if (window.oBXSticker){window.oBXSticker.AddSticker();};BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_5"><span class="bx-panel-small-button-icon bx-panel-small-stickers-icon"></span><span class="bx-panel-small-button-text">Стикеры</span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_5', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Стикеры','TEXT':'Наклеить новый стикер на страницу.','TARGET':'parent'}, GROUP_ID : 5, SKIP : true, LINK: "javascript:void(0)", ACTION : "if (window.oBXSticker){window.oBXSticker.AddSticker();}",TEXT :  "Стикеры" })</script><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_5_menu"><span class="bx-panel-small-button-arrow"></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_5_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Наклеить стикер','TITLE':'Наклеить новый стикер на страницу ( Ctrl+Shift+S ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.AddSticker();}','DEFAULT':true,'HK_ID':'FMST_PANEL_STICKER_ADD'},{'SEPARATOR':true},{'ID':'bxst-show-sticker-icon','TEXT':'Показать стикеры (0)','TITLE':'Отобразить все стикеры на странице ( Ctrl+Shift+X ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowAll();}','HK_ID':'FMST_PANEL_STICKERS_SHOW'},{'TEXT':'Список стикеров страницы','TITLE':'Показать список всех стикеров на текущей странице ( Ctrl+Shift+L ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowList(\'current\');}','HK_ID':'FMST_PANEL_CUR_STICKER_LIST'},{'TEXT':'Список всех стикеров','TITLE':'Показать список всех стикеров на сайте','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowList(\'all\');}','HK_ID':'FMST_PANEL_ALL_STICKER_LIST'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 5, TEXT :  "Стикеры"})</script></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="6"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="/bitrix/admin/wizard_install.php?lang=ru&amp;wizardName=aspro:mshop&amp;wizardSiteID=s1&amp;sessid=6985fd67f6bbfdf811e1f40e7a010051" onclick=";BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_mshop_wizard" title="Запустить мастер смены дизайна и настроек сайта"><span class="bx-panel-button-icon bx-panel-site-wizard-icon"></span></a><a id="bx_topmenu_btn_mshop_wizard_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Мастер<span class="bx-panel-break"></span>настройки&nbsp;<span class="bx-panel-button-arrow"></span></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_mshop_wizard', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', GROUP_ID : 6, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_mshop_wizard_menu', TYPE: 'BIG', MENU: [{'ACTION':'jsUtils.Redirect([], \'/bitrix/admin/wizard_install.php?lang=ru&wizardSiteID=s1&wizardName=aspro:mshop&sessid=6985fd67f6bbfdf811e1f40e7a010051\')','ICON':'bx-popup-item-wizard-icon','TITLE':'Запустить мастер смены дизайна и настройки магазина','TEXT':'<b>Мастер настройки магазина<\/b>'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', GROUP_ID : 6, TEXT :  "Мастер настройки"})</script></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="7"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="/?add_new_site_sol=sol&amp;sessid=6985fd67f6bbfdf811e1f40e7a010051" onclick=";BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_solutions_wizard"><span class="bx-panel-button-icon bx-panel-install-solution-icon"></span></a><a id="bx_topmenu_btn_solutions_wizard_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Протестировать<span class="bx-panel-break"></span>новое решение&nbsp;<span class="bx-panel-button-arrow"></span></span></a><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_solutions_wizard', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Протестировать новое решение','TEXT':'Выбор нового решения для установки.'}, GROUP_ID : 7, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_solutions_wizard_menu', TYPE: 'BIG', MENU: [{'ACTION':'jsUtils.Redirect([], \'/?add_new_site_sol=sol&sessid=6985fd67f6bbfdf811e1f40e7a010051\')','HTML':'<b>Протестировать новое решение<\/b>','TITLE':'Создать новый сайт и запустить мастер установки нового решения'},{'ACTION':'if(confirm(\'Внимание! После удаления кнопки вы сможете запустить мастер создания нового решения в панели управления, используя страницу с мастерами или формой добавления нового сайта. Удалить кнопку с панели?\')) jsUtils.Redirect([], \'/?delete_button_sol=sol&sessid=6985fd67f6bbfdf811e1f40e7a010051\');','TEXT':'Удалить эту кнопку','TITLE':'Удалить эту кнопку с панели'},{'SEPARATOR':true},{'TEXT':'Перейти на сайт','MENU':[{'ACTION':'jsUtils.Redirect([], \'/\');','ICON':'checked','TEXT':'Хризолит','TITLE':'Перейти на сайт Хризолит'}]}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Протестировать новое решение','TEXT':'Нажмите на эту кнопку, чтобы выбрать новое решение для установки, удаление этой кнопки с Панели управления или для перехода на другой сайт.'}, GROUP_ID : 7, TEXT :  "Протестировать новое решение"})</script></span></span></span></div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		BX.admin.panel.state = {
			fixed: false,
			collapsed: true
		};
		BX.admin.moreButton.init({ buttonTitle : "Еще"});
		</script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+75", "location.href=\'/bitrix/admin/hot_keys_list.php?lang=ru\';", 106, 'Перейти в раздел управления горячими клавишами', 18);  BXHotKeys.Add("Ctrl+Alt+80", "BXHotKeys.ShowSettings();", 17, 'Список горячих клавиш на странице', 14);  BXHotKeys.Add("Ctrl+Alt+85", "location.href=\'/bitrix/admin/user_admin.php?lang=\'+phpVars.LANGUAGE_ID;", 139, 'Перейти в раздел управления пользователями', 13); </script><script type='text/javascript'>
			BXHotKeys.MesNotAssign = 'не назначена';
			BXHotKeys.MesClToChange = 'Кликните мышкой, чтобы изменить';
			BXHotKeys.MesClean = 'Очистить';
			BXHotKeys.MesBusy = 'Данная комбинация клавиш уже задействована. Вы уверены, что хотите её использовать повторно?';
			BXHotKeys.MesClose = 'Закрыть';
			BXHotKeys.MesSave = 'Сохранить';
			BXHotKeys.MesSettings = 'Настройка горячих клавиш';
			BXHotKeys.MesDefault = 'По умолчанию';
			BXHotKeys.MesDelAll = 'Удалить все';
			BXHotKeys.MesDelConfirm = 'Вы уверены, что хотите удалить все назначенные сочетания горячих клавиш?';
			BXHotKeys.MesDefaultConfirm = 'Вы уверены, что хотите удалить все назначенные сочетания горячих клавиш и установить сочетания горячих клавиш по умолчанию?';
			BXHotKeys.MesExport = 'Экспорт';
			BXHotKeys.MesImport = 'Импорт';
			BXHotKeys.MesExpFalse = 'Экспортировать горячие клавиши не удалось.';
			BXHotKeys.MesImpFalse = 'Импортировать горячие клавиши не удалось.';
			BXHotKeys.MesImpSuc = 'Успешно импортировано горячих клавиш: ';
			BXHotKeys.MesImpHeader = 'Импорт горячих клавиш';
			BXHotKeys.MesFileEmpty = 'Для импорта горячих клавиш необходимо указать файл.';
			BXHotKeys.MesDelete = 'Удалить';
			BXHotKeys.MesChooseFile = 'Выбрать файл';
			BXHotKeys.uid = 1;
			</script><script type="text/javascript">BX.ready(function(){var BXST_MESS =
{
	Public : "общий",
	Personal : "личный",
	Close : "Отклеить",
	Collapse : "Свернуть",
	UnCollapse : "Восстановить",
	UnCollapseTitle : "Двойной клик для восстановления стикера",
	SetMarkerArea : "Привязать к области на странице",
	SetMarkerEl : "Привязать к элементу на странице",
	Color : "цвет",
	Add : "добавить",
	PersonalTitle : "Только вы можете видеть этот стикер. Нажмите, чтобы изменить.",
	PublicTitle : "Этот стикер могут видеть другие. Нажмите, чтобы изменить.",
	CursorHint : "Выделите область",
	Yellow : "Желтый",
	Green : "Зеленый",
	Blue : "Синий",
	Red : "Красный",
	Purple : "Пурпурный",
	Gray : "Серый",
	StickerListTitle : "Список стикеров",
	CompleteLabel : "выполнено",
	DelConfirm : "Вы уверены, что хотите удалить выбранные стикеры?",
	CloseConfirm : "Этот стикер наклеил #USER_NAME# Вы уверенны, что хотите его отклеить?",
	Complete : "Пометить как завершенное",
	CloseNotify : "Вы можете вернуть отклеенные стикеры на экран из #LINK#списка стикеров#LINK#."
}; window.oBXSticker = new BXSticker({'access':'W','sessid_get':'sessid=6985fd67f6bbfdf811e1f40e7a010051','start_width':'350','start_height':'200','min_width':'280','min_height':'160','start_color':'0','zIndex':'5000','curUserName':'Юка','curUserId':'1','pageUrl':'/local/php_interface/uploadToCSV.php','pageTitle':'/local/php_interface/uploadToCSV.php','bShowStickers':false,'listWidth':'800','listHeight':'450','listNaviSize':'5','useHotkeys':false,'filterParams':{'type':'all','colors':'all','status':'opened','page':'all'},'bHideBottom':true,'focusOnSticker':'0','strDate':'29 Апреля','curPageCount':'0','site_id':'s1'}, [], BXST_MESS);});</script>
<div class="adm-informer" id="admin-informer" style="display: none; top:48px; left:316px;" onclick="return BX.adminInformer.OnInnerClick(event);">
	<div class="adm-informer-header">Новые уведомления</div>
		<div class="adm-informer-item adm-informer-item-red" style="display:block">
			<div class="adm-informer-item-title">
				Автокомпозитный сайт
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-0">
					Уникальная технология позволит ускорить скорость загрузки страниц вашего сайта.
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-0">
				<a href="/bitrix/admin/composite.php?lang=ru">Включить АвтоКомпозит</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-green" style="display:block">
			<div class="adm-informer-item-title">
				Оцените решение Marketplace
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-1">
					Вы используете решение <b>Аспро: Маркет - Адаптивный интернет-магазин</b> (aspro.mshop).<br />Поделитесь своими впечатлениями.<br />
Что понравилось?<br />Есть ли замечания?<br />Ваш отзыв очень важен для нас!
							<script type="text/javascript">
							function hideMpAnswer(el, module)
							{
								if(el.parentNode.parentNode.parentNode)
									BX.hide(el.parentNode.parentNode.parentNode);
									BX.ajax({
										'method': 'POST',
										'dataType': 'html',
										'url': '/bitrix/admin/partner_modules.php',
										'data': 'module='+module+'&sessid=6985fd67f6bbfdf811e1f40e7a010051&act=unnotify',
										'async': true,
										'processData': false

									});
							}
							</script>
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-1">
				<a href="javascript:void(0)" onclick="hideMpAnswer(this, 'aspro.mshop')" style="float: right !important; font-size: 0.8em !important;">Скрыть</a><a href="http://marketplace.1c-bitrix.ru/solutions/aspro.mshop/#tab-rating-link" target="_blank" onclick="hideMpAnswer(this, 'aspro.mshop')">Оставить отзыв</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-peach" style="display:block">
			<div class="adm-informer-item-title">
				Резервное копирование
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-2">
					
				<div class="adm-informer-item-section">
					<span class="adm-informer-item-l">
						<span class="adm-informer-strong-text">Всего:</span> 10 ГБ
					</span>
					<span class="adm-informer-item-r">
							<span class="adm-informer-strong-text">Доступно:</span> 7 ГБ
					</span>
				</div>
				<div class="adm-informer-status-bar-block" >
					<div class="adm-informer-status-bar-indicator" style="width:27%; "></div>
					<div class="adm-informer-status-bar-text">27%</div>
				</div>
			<span class="adm-informer-strong-text">Выполнялось: 70 суток назад.</span>
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-2">
				<a href="/bitrix/admin/dump.php?lang=ru">Рекомендуется выполнить</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-blue" style="display:block">
			<div class="adm-informer-item-title">
				Сканер безопасности
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-3">
					
<div class="adm-informer-item-section">
	<span class="adm-informer-item-l">
		<span>Сканирование выполнялось очень давно либо не выполнялось вовсе</span>
	</span>
</div>

					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-3">
				<a href="/bitrix/admin/security_scanner.php?lang=ru">Перейти к сканированию</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-green" style="display:block">
			<div class="adm-informer-item-title">
				Ускорение сайта (CDN)
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-4">
					
				<div class="adm-informer-item-section">
					<span class="adm-informer-strong-text">Отключено.</span>
				</div>
				<div class="adm-informer-status-bar-block" >
					<div class="adm-informer-status-bar-indicator" style="width:0%; "></div>
					<div class="adm-informer-status-bar-text">0%</div>
				</div>
			
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-4">
				<a href="/bitrix/admin/bitrixcloud_cdn.php?lang=ru">Включить ускорение сайта</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-gray" style="display:none">
			<div class="adm-informer-item-title">
				Обновления
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-5">
					<span class="adm-informer-strong-text">Версия системы 17.0.8</span><br>Последнее обновление<br>28.06.2017 06:49
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-5">
				<a href="/bitrix/admin/update_system.php?refresh=Y&lang=ru">Проверить обновления</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-blue" style="display:none">
			<div class="adm-informer-item-title">
				Проактивная защита
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-6">
					
<div class="adm-informer-item-section">
	<span class="adm-informer-item-l">
		<span class="adm-informer-strong-text">Защита включена.<br></span>
		<span>Попыток вторжения не обнаружено</span>
	</span>
</div>

					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-6">
				<a href="/bitrix/admin/security_filter.php?lang=ru">Настроить защиту</a>
				</div>
			</div>
		</div>
	<a href="javascript:void(0);" class="adm-informer-footer adm-informer-footer-collapsed" hidefocus="true" id="adm-informer-footer" onclick="return BX.adminInformer.ToggleExtra();" >Показать все (7) </a>
	<span class="adm-informer-arrow"></span>
</div>

<script type="text/javascript">
	BX.ready( function(){BX.adminInformer.Init(5); } );
</script></div>
								<script type="text/javascript">
		var arMShopOptions = {};

		BX.message({'MIN_ORDER_PRICE_TEXT':'<b>Минимальная сумма заказа #PRICE#<\/b><br/>\nПожалуйста, добавьте еще товаров в корзину ','LICENSES_TEXT':'Я согласен на <a href=\"/privacy-policy/\" target=\"_blank\">обработку персональных данных<\/a>'});
		</script>
		<!--'start_frame_cache_options-block'-->		<script>
			var arBasketAspro = {'BASKET':{'1347134':'1347134','1343808':'1343808'},'DELAY':{'1348137':'1348137'},'SUBSCRIBE':[],'COMPARE':[]};
			$(document).ready(function() {
				setBasketAspro();
			});
		</script>
		<!--'end_frame_cache_options-block'-->
		<script type="text/javascript">

		var arMShopOptions = ({
			"SITE_ID" : "s1",
			"SITE_DIR" : "/",
			"FORM" : ({
				"ASK_FORM_ID" : "ASK",
				"SERVICES_FORM_ID" : "SERVICES",
				"FEEDBACK_FORM_ID" : "FEEDBACK",
				"CALLBACK_FORM_ID" : "CALLBACK",
				"RESUME_FORM_ID" : "RESUME",
				"TOORDER_FORM_ID" : "TOORDER"
			}),
			"PAGES" : ({
				"FRONT_PAGE" : "",
				"BASKET_PAGE" : "",
				"ORDER_PAGE" : "",
				"PERSONAL_PAGE" : "",
				"CATALOG_PAGE" : ""
			}),
			"PRICES" : ({
				"MIN_PRICE" : "0",
			}),
			"THEME" : ({
				"THEME_SWITCHER" : "n",
				"COLOR_THEME" : "custom",
				"CUSTOM_COLOR_THEME" : "39804b",
				"LOGO_IMAGE" : "",
				"FAVICON_IMAGE" : "/favicon.ico?1587717992",
				"APPLE_TOUCH_ICON_57_IMAGE" : "/include/favicon_57.png",
				"APPLE_TOUCH_ICON_72_IMAGE" : "/include/favicon_72.png",
				"BANNER_WIDTH" : "auto",
				"BANNER_ANIMATIONTYPE" : "SLIDE_HORIZONTAL",
				"BANNER_SLIDESSHOWSPEED" : "5000",
				"BANNER_ANIMATIONSPEED" : "600",
				"HEAD" : ({
					"VALUE" : "type_1",
					"MENU" : "type_1",
					"MENU_COLOR" : "none",
					"HEAD_COLOR" : "white",
				}),
				"BASKET" : "normal",
				"STORES" : "light",
				"STORES_SOURCE" : "iblock",
				"TYPE_SKU" : "type_1",
				"TYPE_VIEW_FILTER" : "vertical",
				"SHOW_BASKET_ONADDTOCART" : "Y",
				"SHOW_ONECLICKBUY_ON_BASKET_PAGE" : "Y",
				"SHOW_BASKET_PRINT" : "N",
				"PHONE_MASK" : "+7 (999) 999-99-99",
				"VALIDATE_PHONE_MASK" : "^[+][7] [(][0-9]{3}[)] [0-9]{3}[-][0-9]{2}[-][0-9]{2}$",
				"SCROLLTOTOP_TYPE" : "ROUND_COLOR",
				"SCROLLTOTOP_POSITION" : "PADDING",
				"SHOW_LICENCE" : "Y",
			}),
			"COUNTERS":({
				"USE_YA_COUNTER" : "N",
				"YANDEX_COUNTER" : "0",
				"YA_COUNTER_ID" : "",
				"YANDEX_ECOMERCE" : "N",
				"USE_FORMS_GOALS" : "COMMON",
				"USE_BASKET_GOALS" : "Y",
				"USE_1CLICK_GOALS" : "Y",
				"USE_FASTORDER_GOALS" : "Y",
				"USE_FULLORDER_GOALS" : "Y",
				"USE_DEBUG_GOALS" : "N",
				"GOOGLE_COUNTER" : "0",
				"GOOGLE_ECOMERCE" : "N",
				"TYPE":{
					"ONE_CLICK":"Покупка в 1 клик",
					"QUICK_ORDER":"Быстрый заказ",
				},
				"GOOGLE_EVENTS":{
					"ADD2BASKET": "addToCart",
					"REMOVE_BASKET": "removeFromCart",
					"CHECKOUT_ORDER": "checkout",
				}
				/*
				"GOALS" : {
					"TO_BASKET": "TO_BASKET",
					"ORDER_START": "ORDER_START",
					"ORDER_SUCCESS": "ORDER_SUCCESS",
					"QUICK_ORDER_SUCCESS": "QUICK_ORDER_SUCCESS",
					"ONE_CLICK_BUY_SUCCESS": "ONE_CLICK_BUY_SUCCESS",
				}
				*/
			}),
			"JS_ITEM_CLICK":({
				"precision" : 6,
				"precisionFactor" : Math.pow(10,6)
			})
		});

		$(document).ready(function(){
			$.extend( $.validator.messages, {
				required: BX.message('JS_REQUIRED'),
				email: BX.message('JS_FORMAT'),
				equalTo: BX.message('JS_PASSWORD_COPY'),
				minlength: BX.message('JS_PASSWORD_LENGTH'),
				remote: BX.message('JS_ERROR')
			});

			$.validator.addMethod(
				'regexp', function( value, element, regexp ){
					var re = new RegExp( regexp );
					return this.optional( element ) || re.test( value );
				},
				BX.message('JS_FORMAT')
			);

			$.validator.addMethod(
				'filesize', function( value, element, param ){
					return this.optional( element ) || ( element.files[0].size <= param )
				},
				BX.message('JS_FILE_SIZE')
			);

			$.validator.addMethod(
				'date', function( value, element, param ) {
					var status = false;
					if(!value || value.length <= 0){
						status = false;
					}
					else{
						// html5 date allways yyyy-mm-dd
						var re = new RegExp('^([0-9]{4})(.)([0-9]{2})(.)([0-9]{2})$');
						var matches = re.exec(value);
						if(matches){
							var composedDate = new Date(matches[1], (matches[3] - 1), matches[5]);
							status = ((composedDate.getMonth() == (matches[3] - 1)) && (composedDate.getDate() == matches[5]) && (composedDate.getFullYear() == matches[1]));
						}
						else{
							// firefox
							var re = new RegExp('^([0-9]{2})(.)([0-9]{2})(.)([0-9]{4})$');
							var matches = re.exec(value);
							if(matches){
								var composedDate = new Date(matches[5], (matches[3] - 1), matches[1]);
								status = ((composedDate.getMonth() == (matches[3] - 1)) && (composedDate.getDate() == matches[1]) && (composedDate.getFullYear() == matches[5]));
							}
						}
					}
					return status;
				}, BX.message('JS_DATE')
			);

			$.validator.addMethod(
				'extension', function(value, element, param){
					param = typeof param === 'string' ? param.replace(/,/g, '|') : 'png|jpe?g|gif';
					return this.optional(element) || value.match(new RegExp('.(' + param + ')$', 'i'));
				}, BX.message('JS_FILE_EXT')
			);

			$.validator.addMethod(
				'captcha', function( value, element, params ){
					return $.validator.methods.remote.call(this, value, element,{
						url: arMShopOptions['SITE_DIR'] + 'ajax/check-captcha.php',
						type: 'post',
						data:{
							captcha_word: value,
							captcha_sid: function(){
								return $(element).closest('form').find('input[name="captcha_sid"]').val();
							}
						}
					});
				},
				BX.message('JS_ERROR')
			);

			$.validator.addClassRules({
				'phone':{
					regexp: arMShopOptions['THEME']['VALIDATE_PHONE_MASK']
				},
				'confirm_password':{
					equalTo: 'input[name="REGISTER\[PASSWORD\]"]',
					minlength: 6
				},
				'password':{
					minlength: 6
				},
				'inputfile':{
					extension: arMShopOptions['THEME']['VALIDATE_FILE_EXT'],
					filesize: 5000000
				},
				'captcha':{
					captcha: ''
				}
			});

			if(arMShopOptions['THEME']['PHONE_MASK']){
				$('input.phone').inputmask('mask', {'mask': arMShopOptions['THEME']['PHONE_MASK']});
			}

			jqmEd('feedback', arMShopOptions['FORM']['FEEDBACK_FORM_ID']);
			jqmEd('ask', arMShopOptions['FORM']['ASK_FORM_ID'], '.ask_btn');
			jqmEd('services', arMShopOptions['FORM']['SERVICES_FORM_ID'], '.services_btn','','.services_btn');
			if($('.resume_send').length){
				$('.resume_send').live('click', function(e){
					$("body").append("<span class='resume_send_wr' style='display:none;'></span>");
					jqmEd('resume', arMShopOptions['FORM']['RESUME_FORM_ID'], '.resume_send_wr','', this);
					$("body .resume_send_wr").click();
					$("body .resume_send_wr").remove();
				})
			}
			jqmEd('callback', arMShopOptions['FORM']['CALLBACK_FORM_ID'], '.callback_btn');
		});
		</script>

															<script type="text/javascript">
				function jsPriceFormat(_number){
					BX.Currency.setCurrencyFormat('RUB', {'CURRENCY':'RUB','LID':'ru','FORMAT_STRING':'# руб.','FULL_NAME':'Рубль','DEC_POINT':'.','THOUSANDS_SEP':' ','DECIMALS':'2','THOUSANDS_VARIANT':'S','HIDE_ZERO':'Y','CREATED_BY':'','DATE_CREATE':'2017-04-10 12:29:58','MODIFIED_BY':'1','TIMESTAMP_X':'2017-04-10 12:48:50'});
					return BX.Currency.currencyFormat(_number, 'RUB', true);
				}
				</script>
					
						
				
								<div class="wrapper  h_color_white m_color_none  basket_normal head_type_1 banner_auto">
			<div class="header_wrap ">
				<div class="top-h-row">
					<div class="wrapper_inner">
						<div class="content_menu">
								<ul class="menu">
					<li  >
				<a href="/partners/"><span>Партнеры</span></a>
			</li>
					<li  >
				<a href="/company/"><span>О компании</span></a>
			</li>
					<li  >
				<a href="/bonusnaya-programma/"><span>Бонусная программа</span></a>
			</li>
					<li  >
				<a href="/help/payment/"><span>Оплата и доставка</span></a>
			</li>
					<li  >
				<a href="/help/delivery/"><span>Гарантии</span></a>
			</li>
					<li  >
				<a href="/help/warranty/"><span>Обмен</span></a>
			</li>
					<li  >
				<a href="/contacts/stores/"><span>Магазины</span></a>
			</li>
					<li  >
				<a href="/contacts/"><span>Контакты</span></a>
			</li>
			</ul>
	<script>
		$(".content_menu .menu > li:not(.current) > a").click(function()
		{
			$(this).parents("li").siblings().removeClass("current");
			$(this).parents("li").addClass("current");
		});
	</script>
						</div>
						   
						<div class="h-user-block" id="personal_block">
							<div class="form_mobile_block"><div class="search_middle_block">	<div id="title-search3" class="stitle_form">
		<form action="/catalog/">
			<div class="form-control1 bg">
				<input id="title-search-input3" type="text" name="q" value="" size="40" class="text small_block" maxlength="50" autocomplete="off" placeholder="Поиск по сайту" /><input name="s" type="submit" value="Поиск" class="button icon" />
			</div>
		</form>
	</div>
<script type="text/javascript">
var jsControl = new JCTitleSearch({
	//'WAIT_IMAGE': '/bitrix/themes/.default/images/wait.gif',
	'AJAX_PAGE' : '/local/php_interface/uploadToCSV.php?clear_cache=Y',
	'CONTAINER_ID': 'title-search3',
	'INPUT_ID': 'title-search-input3',
	'MIN_QUERY_LEN': 2
});
$("#title-search-input3").focus(function() { $(this).parents("form").find("button[type='submit']").addClass("hover"); });
$("#title-search-input3").blur(function() { $(this).parents("form").find("button[type='submit']").removeClass("hover"); });
</script>
</div></div>
							<form id="auth_params" action="/ajax/show_personal_block.php">
	<input type="hidden" name="REGISTER_URL" value="/auth/registration/" />
	<input type="hidden" name="FORGOT_PASSWORD_URL" value="/auth/forgot-password/" />
	<input type="hidden" name="PROFILE_URL" value="/personal/" />
	<input type="hidden" name="SHOW_ERRORS" value="Y" />
</form>
<!--'start_frame_cache_iIjGFB'-->	<div class="module-enter have-user">
		<!--noindex-->
			<a href="/personal/?backurl=%2Flocal%2Fphp_interface%2FuploadToCSV.php%3Fclear_cache%3DY" class="reg" rel="nofollow"><span>Личный кабинет</span></a>
			<a href="/?logout=yes" class="exit_link" rel="nofollow"><span>Выход</span></a>
		<!--/noindex-->
	</div>	
<!--'end_frame_cache_iIjGFB'-->						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<header id="header">
					<div class="wrapper_inner">	
						<table class="middle-h-row">
						<tr>
							<td class="logo_wrapp">
								<div class="logo">
											<a href="/"><img src="" alt="Хризолит" title="Хризолит" /></a>
		<a href="/" class="print_img"><img src="" alt="Хризолит" title="Хризолит" /></a>
										</div>
							</td>
							<td  class="center_block">
								
								
								<div class="middle_phone">
									<div class="phones">
										<span class="phone_wrap">
											<span class="icons"></span>
											<span class="phone_text">
												<a rel="nofollow" href="tel:+7–913–746–55–16">+7–913–746–55–16</a>											</span>
										</span>
										<span class="order_wrap_btn">
																							<span class="callback_btn">Заказать звонок</span>
																					</span>
									</div>
								</div>
								<div class="search">
										<div id="title-search" class="stitle_form">
		<form action="/catalog/">
			<div class="form-control1 bg">
				<input id="title-search-input" type="text" name="q" value="" size="40" class="text small_block" maxlength="50" autocomplete="off" placeholder="Поиск по сайту" /><input name="s" type="submit" value="Поиск" class="button icon" />
			</div>
		</form>
	</div>
<script type="text/javascript">
var jsControl = new JCTitleSearch({
	//'WAIT_IMAGE': '/bitrix/themes/.default/images/wait.gif',
	'AJAX_PAGE' : '/local/php_interface/uploadToCSV.php?clear_cache=Y',
	'CONTAINER_ID': 'title-search',
	'INPUT_ID': 'title-search-input',
	'MIN_QUERY_LEN': 2
});
$("#title-search-input").focus(function() { $(this).parents("form").find("button[type='submit']").addClass("hover"); });
$("#title-search-input").blur(function() { $(this).parents("form").find("button[type='submit']").removeClass("hover"); });
</script>
								</div>
							</td>
							<td class="basket_wrapp custom_basket_class ">
								<div class="wrapp_all_icons">
										<div class="header-compare-block icon_block iblock" id="compare_line">
											<!--'start_frame_cache_header-compare-block'--><!--noindex-->
<div class="wraps_icon_block">
	<a href="/catalog/compare.php?action=COMPARE"style="display:none;" class="link" title="Список сравниваемых товаров"></a>
	<div class="count">
		<span>
			<span class="items">
				<span class="text">0</span>
			</span>
		</span>
	</div>
</div>
<div class="clearfix"></div>
<!--/noindex--><!--'end_frame_cache_header-compare-block'-->										</div>
										<div class="header-cart" id="basket_line">
											<!--'start_frame_cache_header-cart'-->																																		
<div class="basket_normal cart   ">
	<!--noindex-->
		<div class="wraps_icon_block delay ">
			<a href="/basket/#tab_DelDelCanBuy" class="link"  title="Список отложенных товаров"></a>
			<div class="count">
				<span>
					<span class="items">
						<span class="text">1</span>
					</span>
				</span>
			</div>
		</div>
		<div class="basket_block f-left">
			<a href="/basket/" class="link" title="Список товаров в корзине"></a>
			<div class="wraps_icon_block basket">
				<a href="/basket/" class="link" title="Список товаров в корзине"></a>
				<div class="count">
					<span>
						<span class="items">
							<a href="/basket/">2</a>
						</span>
					</span>
				</div>
			</div>
			<div class="text f-left">
				<div class="title">Корзина</div>
				<div class="value">
											87 630 руб.									</div>
			</div>
			<div class="card_popup_frame popup">
				<div class="basket_popup_wrapper">
					<div class="basket_popup_wrapp" >
						<div class="cart_wrapper" >
							<table class="cart_shell">
								<tbody>
																				<tr class="catalog_item" product-id="112" data-iblockid="15" data-offer="Y" catalog-product-id="1343808">
												<td class="thumb-cell">
													<a href="">
																													<img src="/upload/iblock/a1e/braslet-511064p-_585_511064p_585_granat.jpg" alt="Браслет 511064п (585) (3102111339462       )" title="Браслет 511064п (585) (3102111339462       )" />
																											</a>
												</td>
												<td class="item-title">
													<a href="" class="clearfix"><span>Браслет 511064п (585) (3102111339462       )</span></a>
																											<div class="props">
																															<div class="item_prop"><span class="title">Размер:</span><span class="value">21.0</span></div>
																															<div class="item_prop"><span class="title">Металл:</span><span class="value">Золото</span></div>
																															<div class="item_prop"><span class="title">Проба:</span><span class="value">585</span></div>
																															<div class="item_prop"><span class="title">Камень:</span><span class="value">Гранат</span></div>
																															<div class="item_prop"><span class="title">Вставка:</span><span class="value">18 Гранат капля 9.0*6.0  (5,4038)</span></div>
																															<div class="item_prop"><span class="title">Вес:</span><span class="value">13,58</span></div>
																													</div>
																																							<div class="one-item">
																												<span class="value">66 190 руб.</span>
														<span class="measure">x <span>1</span> шт.</span>
													</div>
													<div class="cost-cell">														
														<input type="hidden" name="item_one_price_112" value="">
														<input type="hidden" name="item_one_price_discount_112" value="0">
														<input type="hidden" name="item_price_112" value="66190">
														<input type="hidden" name="item_price_discount_112" value="0">														
														<span class="price">66 190 руб.</span>
													</div>
													<div class="clearfix"></div>
													<!--noindex-->
														<div class="remove-cell">
															<span class="remove" data-id="112" rel="nofollow" href="/basket/?action=delete&id=112" title="Удалить"><i></i></span>
														</div>
													<!--/noindex-->
												</td>
											</tr>
																														<tr class="catalog_item" product-id="110" data-iblockid="15" data-offer="Y" catalog-product-id="1347134">
												<td class="thumb-cell">
													<a href="">
																													<img src="/upload/iblock/b26/koltso-k-354p-_585_k-354p_585_fianit.jpg" alt="Кольцо К 354п (585) (3103261560096       )" title="Кольцо К 354п (585) (3103261560096       )" />
																											</a>
												</td>
												<td class="item-title">
													<a href="" class="clearfix"><span>Кольцо К 354п (585) (3103261560096       )</span></a>
																											<div class="props">
																															<div class="item_prop"><span class="title">Размер:</span><span class="value">18.5</span></div>
																															<div class="item_prop"><span class="title">Металл:</span><span class="value">Золото</span></div>
																															<div class="item_prop"><span class="title">Проба:</span><span class="value">585</span></div>
																															<div class="item_prop"><span class="title">Камень:</span><span class="value">Фианит</span></div>
																															<div class="item_prop"><span class="title">Вставка:</span><span class="value">1 Ц 7.0  (0,4114), 91 Ц 1.0  (0,1638)</span></div>
																															<div class="item_prop"><span class="title">Вес:</span><span class="value">4,84</span></div>
																															<div class="item_prop"><span class="title">Цвет:</span><span class="value">бесцветный</span></div>
																													</div>
																																							<div class="one-item">
																												<span class="value">21 440 руб.</span>
														<span class="measure">x <span>1</span> шт.</span>
													</div>
													<div class="cost-cell">														
														<input type="hidden" name="item_one_price_110" value="">
														<input type="hidden" name="item_one_price_discount_110" value="0">
														<input type="hidden" name="item_price_110" value="21440">
														<input type="hidden" name="item_price_discount_110" value="0">														
														<span class="price">21 440 руб.</span>
													</div>
													<div class="clearfix"></div>
													<!--noindex-->
														<div class="remove-cell">
															<span class="remove" data-id="110" rel="nofollow" href="/basket/?action=delete&id=110" title="Удалить"><i></i></span>
														</div>
													<!--/noindex-->
												</td>
											</tr>
																											</tbody>
							</table>
						</div>
						<div class="basket_empty clearfix">
							<table>
								<tr>
									<td class="image"><div></div></td>
									<td class="description"><div class="basket_empty_subtitle">К сожалению, ваша корзина пуста.</div><div class="basket_empty_description">Исправить это недоразумение очень просто:<br />выберите в каталоге интересующий товар и нажмите кнопку &laquo;В корзину&raquo;.</div></td>
								</tr>
							</table>
						</div>
						<div class="total_wrapp clearfix">
							<div class="total"><span>Общая сумма:</span><span class="price">87 630 руб.</span><div class="clearfix"></div></div>
							<input type="hidden" name="total_price" value="87630" />
							<input type="hidden" name="total_count" value="2" />
							<input type="hidden" name="delay_count" value="1" />
							<div class="but_row1">
								<a href="/basket/" class="button short"><span class="text">Перейти в корзину</span></a>
							</div>
						</div>
												<input id="top_basket_params" type="hidden" name="PARAMS" value='a%3A49%3A%7Bs%3A14%3A%22PATH_TO_BASKET%22%3Bs%3A8%3A%22%2Fbasket%2F%22%3Bs%3A13%3A%22PATH_TO_ORDER%22%3Bs%3A7%3A%22%2Forder%2F%22%3Bs%3A10%3A%22SHOW_DELAY%22%3Bs%3A1%3A%22Y%22%3Bs%3A13%3A%22SHOW_PRODUCTS%22%3Bs%3A1%3A%22Y%22%3Bs%3A17%3A%22SHOW_EMPTY_VALUES%22%3Bs%3A1%3A%22Y%22%3Bs%3A13%3A%22SHOW_NOTAVAIL%22%3Bs%3A1%3A%22N%22%3Bs%3A14%3A%22SHOW_SUBSCRIBE%22%3Bs%3A1%3A%22N%22%3Bs%3A10%3A%22SHOW_IMAGE%22%3Bs%3A1%3A%22Y%22%3Bs%3A10%3A%22SHOW_PRICE%22%3Bs%3A1%3A%22Y%22%3Bs%3A12%3A%22SHOW_SUMMARY%22%3Bs%3A1%3A%22Y%22%3Bs%3A17%3A%22SHOW_NUM_PRODUCTS%22%3Bs%3A1%3A%22Y%22%3Bs%3A16%3A%22SHOW_TOTAL_PRICE%22%3Bs%3A1%3A%22Y%22%3Bs%3A10%3A%22CACHE_TYPE%22%3Bs%3A1%3A%22A%22%3Bs%3A20%3A%22HIDE_ON_BASKET_PAGES%22%3Bs%3A1%3A%22Y%22%3Bs%3A18%3A%22SHOW_PERSONAL_LINK%22%3Bs%3A1%3A%22N%22%3Bs%3A16%3A%22PATH_TO_PERSONAL%22%3Bs%3A10%3A%22%2Fpersonal%2F%22%3Bs%3A11%3A%22SHOW_AUTHOR%22%3Bs%3A1%3A%22N%22%3Bs%3A16%3A%22PATH_TO_REGISTER%22%3Bs%3A7%3A%22%2Flogin%2F%22%3Bs%3A17%3A%22PATH_TO_AUTHORIZE%22%3Bs%3A7%3A%22%2Flogin%2F%22%3Bs%3A15%3A%22PATH_TO_PROFILE%22%3Bs%3A10%3A%22%2Fpersonal%2F%22%3Bs%3A14%3A%22POSITION_FIXED%22%3Bs%3A1%3A%22N%22%3Bs%3A17%3A%22POSITION_VERTICAL%22%3Bs%3A3%3A%22top%22%3Bs%3A19%3A%22POSITION_HORIZONTAL%22%3Bs%3A5%3A%22right%22%3Bs%3A4%3A%22AJAX%22%3Bs%3A1%3A%22N%22%3Bs%3A15%3A%22%7EPATH_TO_BASKET%22%3Bs%3A8%3A%22%2Fbasket%2F%22%3Bs%3A14%3A%22%7EPATH_TO_ORDER%22%3Bs%3A7%3A%22%2Forder%2F%22%3Bs%3A11%3A%22%7ESHOW_DELAY%22%3Bs%3A1%3A%22Y%22%3Bs%3A14%3A%22%7ESHOW_PRODUCTS%22%3Bs%3A1%3A%22Y%22%3Bs%3A18%3A%22%7ESHOW_EMPTY_VALUES%22%3Bs%3A1%3A%22Y%22%3Bs%3A14%3A%22%7ESHOW_NOTAVAIL%22%3Bs%3A1%3A%22N%22%3Bs%3A15%3A%22%7ESHOW_SUBSCRIBE%22%3Bs%3A1%3A%22N%22%3Bs%3A11%3A%22%7ESHOW_IMAGE%22%3Bs%3A1%3A%22Y%22%3Bs%3A11%3A%22%7ESHOW_PRICE%22%3Bs%3A1%3A%22Y%22%3Bs%3A13%3A%22%7ESHOW_SUMMARY%22%3Bs%3A1%3A%22Y%22%3Bs%3A18%3A%22%7ESHOW_NUM_PRODUCTS%22%3Bs%3A1%3A%22Y%22%3Bs%3A17%3A%22%7ESHOW_TOTAL_PRICE%22%3Bs%3A1%3A%22Y%22%3Bs%3A11%3A%22%7ECACHE_TYPE%22%3Bs%3A1%3A%22A%22%3Bs%3A21%3A%22%7EHIDE_ON_BASKET_PAGES%22%3Bs%3A1%3A%22Y%22%3Bs%3A19%3A%22%7ESHOW_PERSONAL_LINK%22%3Bs%3A1%3A%22N%22%3Bs%3A17%3A%22%7EPATH_TO_PERSONAL%22%3Bs%3A10%3A%22%2Fpersonal%2F%22%3Bs%3A12%3A%22%7ESHOW_AUTHOR%22%3Bs%3A1%3A%22N%22%3Bs%3A17%3A%22%7EPATH_TO_REGISTER%22%3Bs%3A7%3A%22%2Flogin%2F%22%3Bs%3A18%3A%22%7EPATH_TO_AUTHORIZE%22%3Bs%3A7%3A%22%2Flogin%2F%22%3Bs%3A16%3A%22%7EPATH_TO_PROFILE%22%3Bs%3A10%3A%22%2Fpersonal%2F%22%3Bs%3A15%3A%22%7EPOSITION_FIXED%22%3Bs%3A1%3A%22N%22%3Bs%3A18%3A%22%7EPOSITION_VERTICAL%22%3Bs%3A3%3A%22top%22%3Bs%3A20%3A%22%7EPOSITION_HORIZONTAL%22%3Bs%3A5%3A%22right%22%3Bs%3A5%3A%22%7EAJAX%22%3Bs%3A1%3A%22N%22%3Bs%3A6%3A%22cartId%22%3Bs%3A10%3A%22bx_basket1%22%3B%7D' />
					</div>
				</div>
			</div>
		</div>
	<script type="text/javascript">
	$('.card_popup_frame').ready(function(){
		$('.card_popup_frame span.remove').click(function(e){
			e.preventDefault();
			if(!$(this).is(".disabled")){
				var row = $(this).parents("tr").first();
				row.fadeTo(100 , 0.05, function() {});
				delFromBasketCounter($(this).closest('tr').attr('catalog-product-id'));
				reloadTopBasket('del', $('#basket_line'), 200, 2000, 'N', $(this));
				markProductRemoveBasket($(this).closest('.catalog_item').attr('catalog-product-id'));
			}
		});
	});
	</script>
</div>

																						<!--'end_frame_cache_header-cart'-->										</div>
									</div>
									<div class="clearfix"></div>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<div class="main-nav">
										<ul class="menu adaptive">
		<li class="menu_opener"><a>Меню</a><i class="icon"></i></li>
	</ul>
	<ul class="menu full">
					<li class="menu_item_l1 ">
				<a href="/sale/">
					<span>Акции</span>
				</a>
											</li>
					<li class="menu_item_l1  catalog">
				<a href="/catalog/">
					<span>Украшения</span>
				</a>
													<div class="child cat_menu">
	<div class="child_wrapp">
												<ul >
				<li class="menu_title"><a href="/catalog/braslet/">Браслет</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/brosh/">Брошь</a></li>
							</ul>
								<ul class="last">
				<li class="menu_title"><a href="/catalog/zazhim/">Зажим</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/zaponki/">Запонки</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/ikona/">Икона</a></li>
							</ul>
								<ul class="last">
				<li class="menu_title"><a href="/catalog/kole/">Колье</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/koltso/">Кольцо</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/krest/">Крест</a></li>
							</ul>
								<ul class="last">
				<li class="menu_title"><a href="/catalog/lozhka/">Ложка</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/pechatka/">Печатка</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/podveska/">Подвеска</a></li>
							</ul>
								<ul class="last">
				<li class="menu_title"><a href="/catalog/remeshok/">Ремешок</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/sergi/">Серьги</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/suvenir/">Сувенир</a></li>
							</ul>
								<ul class="last">
				<li class="menu_title"><a href="/catalog/tsep/">Цепь</a></li>
							</ul>
								<ul >
				<li class="menu_title"><a href="/catalog/shnurok/">Шнурок</a></li>
							</ul>
			</div>
</div>
							</li>
					<li class="menu_item_l1 ">
				<a href="/company/news/">
					<span>По знакам зодиака</span>
				</a>
											</li>
					<li class="menu_item_l1 ">
				<a href="/auth/">
					<span>Оптовикам</span>
				</a>
											</li>
				<li class="stretch"></li>
		<li class="search_row">
			<form action="/catalog/" class="search1">
	<input id="title-search-input4" class="search_field1" type="text" name="q" placeholder="Поиск по сайту" autocomplete="off" />
	<button id="search-submit-button" type="submit" class="submit"><i></i></button>
			<div id="title-search4"></div>
		<script type="text/javascript">
var jsControl = new JCTitleSearch({
	//'WAIT_IMAGE': '/bitrix/themes/.default/images/wait.gif',
	'AJAX_PAGE' : '/local/php_interface/uploadToCSV.php?clear_cache=Y',
	'CONTAINER_ID': 'title-search4',
	'INPUT_ID': 'title-search-input4',
	'MIN_QUERY_LEN': 2
});
$("#title-search-input4").focus(function() { $(this).parents("form").find("button[type='submit']").addClass("hover"); });
$("#title-search-input4").blur(function() { $(this).parents("form").find("button[type='submit']").removeClass("hover"); });
</script>
	</form>		</li>
	</ul>
		<div class="search_middle_block">
			<div id="title-search2" class="middle_form">
	<form action="/catalog/">
		<div class="form-control1 bg">
			<input id="title-search-input2" type="text" name="q" value="" size="40" class="text big" maxlength="50" autocomplete="off" placeholder="Поиск по сайту" /><input name="s" type="submit" value="Поиск" class="button noborder" />
		</div>
	</form>
	</div>
<script type="text/javascript">
var jsControl = new JCTitleSearch({
	//'WAIT_IMAGE': '/bitrix/themes/.default/images/wait.gif',
	'AJAX_PAGE' : '/local/php_interface/uploadToCSV.php?clear_cache=Y',
	'CONTAINER_ID': 'title-search2',
	'INPUT_ID': 'title-search-input2',
	'MIN_QUERY_LEN': 2
});
$("#title-search-input2").focus(function() { $(this).parents("form").find("button[type='submit']").addClass("hover"); });
$("#title-search-input2").blur(function() { $(this).parents("form").find("button[type='submit']").removeClass("hover"); });
</script>
	</div>
	<div class="search_block">
		<span class="icon"></span>
	</div>
	<script type="text/javascript">
	$(document).ready(function() {
		
		
		$(".main-nav .menu > li:not(.current):not(.menu_opener) > a").click(function(){
			$(this).parents("li").siblings().removeClass("current");
			$(this).parents("li").addClass("current");
		});
		
		$(".main-nav .menu .child_wrapp a").click(function(){
			$(this).siblings().removeClass("current");
			$(this).addClass("current");
		});
	});
	</script>
								</div>
							</td>
						</tr>
						</table>
					</div>
					<div class="catalog_menu">
						<div class="wrapper_inner">
							<div class="wrapper_middle_menu">
									<ul class="menu bottom">
									<li class="menu_item_l1  first active">
					<a class="" href="/sale/">Акции</a>
									</li>
							<li class="more menu_item_l1">
			<a>Еще<i></i></a>
			<div class="child cat_menu">
				<div class="child_wrapp">
									</div>
			</div>
		</li>
		<li class="stretch"></li>
	</ul>
	<script type="text/javascript">
	// menu block
	var nodeCatalogMenu = document.querySelector('.catalog_menu .menu')
	// last menu width when it was calculated
	nodeCatalogMenu.lastCalculatedWidth = false

	// menu item MORE
	var nodeMore = nodeCatalogMenu.querySelector('li.more')
	// and it`s width
	var moreWidth = nodeMore.offsetWidth
	// and it`s submenu with childs
	var nodeMoreSubmenu = nodeMore.querySelector('.child_wrapp')

	var reCalculateMenu = function(){
		// get current menu width
		var menuWidth = nodeCatalogMenu.offsetWidth
		// and compare wth last width when it was calculated
		if(menuWidth !== nodeCatalogMenu.lastCalculatedWidth){
			nodeCatalogMenu.lastCalculatedWidth = menuWidth

			// clear menu item MORE submenu
						nodeMoreSubmenu.innerHTML = ''
			nodeMore.classList.remove('visible')
						// and hide this item
			// show all root items of menu which was hided at last calculate
			Array.prototype.slice.call(document.querySelectorAll('.catalog_menu .menu > li:not(.stretch)')).forEach(function(node){
				node.style.display = 'inline-block'
			})
			nodeCatalogMenu.style.display = 'block'

			// last index of root items of menu without items MORE & STRETCH
			var lastIndex = $('.catalog_menu .menu > li:not(.more):not(.stretch)').length - 1
			// count of items that cloned to item`s MORE submenu
			var cntItemsInMore = 0;
			var cntMinItemsInMore = cntItemsInMore
			// get all root items of menu without items MORE & STRETCH and do something
			Array.prototype.slice.call(document.querySelectorAll('.catalog_menu .menu > li:not(.more):not(.stretch)')).forEach(function(node, i){
				// is it last root item of menu?
				var bLast = lastIndex === i
				// it`s width
				var itemWidth = node.offsetWidth
				// if item MORE submenu is not empty OR overflow than clone item
				if((cntItemsInMore > cntMinItemsInMore) || (node.offsetLeft + itemWidth + (bLast ? 0 : moreWidth) > menuWidth)){
					// show item MORE if it was empty
					if(!cntItemsInMore++){
						nodeMore.classList.add('visible')
						nodeMore.style.display = 'inline-block'
					}

					// clone item
					var nodeClone = node.cloneNode(true)
					// and hide it
					node.style.display = 'none'

					// wrap cloned item
					var nodeWrap = document.createElement('div')
					nodeWrap.appendChild(nodeClone)
					delete node
					node = nodeWrap.querySelector('.menu_item_l1')

					// replace cloned item childs structure
					var nodeLink = nodeWrap.querySelector('.menu_item_l1 > a')
					if(nodeLink){
						var hrefLink = nodeLink.getAttribute('href')
						var textLink = nodeLink.innerText
						var p = nodeLink.parentNode
						nodeLink.parentNode.removeChild(nodeLink)
					}
					Array.prototype.slice.call(nodeClone.querySelectorAll('.depth3 a:not(.title)')).forEach(function(_node){
						_node.parentNode.removeChild(_node)
					})
					$(node).wrapInner('<ul class="cloned"></ul>')
					var nodeUL = node.querySelector('ul')
					var nodeLI = document.createElement('li')
					var addClass = node.className.replace('menu_item_l1', '').trim()
					nodeLI.classList.add('menu_title')
					if(addClass.length){
						nodeLI.classList.add(addClass)
					}
					nodeLI.innerHTML = '<a href="' + (hrefLink && hrefLink.trim().length ? hrefLink : '') + '">' + textLink + '</a>'
					if(nodeUL.childNodes.length){
						nodeUL.insertBefore(nodeLI, nodeUL.childNodes[0])
					}
					else{
						nodeUL.appendChild(nodeLI)
					}
					Array.prototype.slice.call(node.querySelectorAll('.child_wrapp > a,.child_wrapp > .depth3 a.title')).forEach(function(_node){
						$(_node).wrap('<li class="menu_item"></li>')
					})
					Array.prototype.slice.call(node.querySelectorAll('li.menu_item')).forEach(function(_node){
						if(nodeUL){
							var $a = $(_node).find('> a')
							if($a.length){
								var nodeA = $a[0]
								var classA = nodeA.className
								var styleA = nodeA.getAttribute('style')
								nodeUL.innerHTML = nodeUL.innerHTML + '<li class="menu_item' + ((classA && classA.trim().length) ? ' ' + classA.trim() : '') + '"' + ((styleA && styleA.trim().length) ? 'style="' + styleA.trim() + '"' : '') + '>' + _node.innerHTML + '</li>'
							}
						}
					})
					Array.prototype.slice.call(node.querySelectorAll('.child.submenu')).forEach(function(_node){
						_node.parentNode.removeChild(_node)
					})

					// append cloned item html to item MORE submenu
											nodeMoreSubmenu.appendChild(nodeUL)
									}
				else{
					// align child menu of root items
					if(i){
						var nodesSubmenu = node.getElementsByClassName('submenu')
						if(nodesSubmenu.length){
							nodesSubmenu[0].style.marginLeft = (itemWidth - $(nodesSubmenu[0]).outerWidth()) / 2 + 'px'
						}
					}

					// show this item
					node.style.display = 'inline-block'
					// remove left border
					if(bLast){
						node.style.borderLeftWidth = '0px'
					}
				}
			});

			// hide item MORE if it`s submenu is empty
			if(!cntItemsInMore){
				nodeMore.style.display = 'none'
			}
			else{
				// or set class "last" for even 3 item in submenu
				Array.prototype.slice.call(nodeMoreSubmenu.querySelectorAll('ul')).forEach(function(node, i){
					if(i % 3){
						node.classList.remove('last')
					}
					else{
						node.classList.add('last')
					}
				})
			}

			// I don`t know what is it
			Array.prototype.slice.call(nodeMore.querySelectorAll('.see_more a.see_more')).forEach(function(node){
				node.classList.remove('see_more')
			})
			Array.prototype.slice.call(nodeMore.querySelectorAll('li.menu_item a')).forEach(function(node){
				node.classList.remove('d')
			})
			Array.prototype.slice.call(nodeMore.querySelectorAll('li.menu_item a')).forEach(function(node){
				node.removeAttribute('style')
			})
		}
	}

	$(document).ready(function() {
		if($(window).outerWidth() > 600){
			reCalculateMenu()
		}
	});
	</script>
							</div>
						</div>
					</div>
				</header>
			</div>
							<div class="wrapper_inner">				
					<section class="middle">
						<div class="container">
							<div class="breadcrumbs" id="navigation" itemscope="" itemtype="http://schema.org/BreadcrumbList"><div class="bx-breadcrumb-item" id="bx_breadcrumb_0" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/" title="Главная" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a></div></div>							<h1 id="pagetitle"></h1>
											<div id="content">
																						<div style="margin: 20px; background: #fdf5db; border-radius: 4px; padding: 5px; border: 1px solid #ffe69d; box-shadow: 0 0 10px 0px #ccc; font-family: Arial;"><div style="margin-bottom: 5px; font-size: 11px; color: #848484; overflow:hidden;" title="File /var/www/html/local/php_interface/uploadToCSV.php in line 28">File <b>/var/www/html/local/php_interface/uploadToCSV.php</b> in line <b>28</b></div><pre style="display: block; margin: 0; padding: 10px; background: #fff; border: 1px solid #ccc; max-height: 400px; overflow: scroll; font-size: 13px; color: #000;">Array
(
    [name] => 
    [key] => Выгрузить
)
</pre></div><div style="margin: 20px; background: #fdf5db; border-radius: 4px; padding: 5px; border: 1px solid #ffe69d; box-shadow: 0 0 10px 0px #ccc; font-family: Arial;"><div style="margin-bottom: 5px; font-size: 11px; color: #848484; overflow:hidden;" title="File /var/www/html/local/php_interface/uploadToCSV.php in line 30">File <b>/var/www/html/local/php_interface/uploadToCSV.php</b> in line <b>30</b></div><pre style="display: block; margin: 0; padding: 10px; background: #fff; border: 1px solid #ccc; max-height: 400px; overflow: scroll; font-size: 13px; color: #000;">begin</pre></div><div style="margin: 20px; background: #fdf5db; border-radius: 4px; padding: 5px; border: 1px solid #ffe69d; box-shadow: 0 0 10px 0px #ccc; font-family: Arial;"><div style="margin-bottom: 5px; font-size: 11px; color: #848484; overflow:hidden;" title="File /var/www/html/local/php_interface/uploadToCSV.php in line 32">File <b>/var/www/html/local/php_interface/uploadToCSV.php</b> in line <b>32</b></div><pre style="display: block; margin: 0; padding: 10px; background: #fff; border: 1px solid #ccc; max-height: 400px; overflow: scroll; font-size: 13px; color: #000;">trade.csv</pre></div>nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
<div style="margin: 20px; background: #fdf5db; border-radius: 4px; padding: 5px; border: 1px solid #ffe69d; box-shadow: 0 0 10px 0px #ccc; font-family: Arial;"><div style="margin-bottom: 5px; font-size: 11px; color: #848484; overflow:hidden;" title="File /var/www/html/local/php_interface/uploadToCSV.php in line 54">File <b>/var/www/html/local/php_interface/uploadToCSV.php</b> in line <b>54</b></div><pre style="display: block; margin: 0; padding: 10px; background: #fff; border: 1px solid #ccc; max-height: 400px; overflow: scroll; font-size: 13px; color: #000;">936</pre></div>nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
nameProduct;xml_id_product;nameTrade;xml_id_trade;warehouse;xml_id_wh;balance
<div style="margin: 20px; background: #fdf5db; border-radius: 4px; padding: 5px; border: 1px solid #ffe69d; box-shadow: 0 0 10px 0px #ccc; font-family: Arial;"><div style="margin-bottom: 5px; font-size: 11px; color: #848484; overflow:hidden;" title="File /var/www/html/local/php_interface/uploadToCSV.php in line 56">File <b>/var/www/html/local/php_interface/uploadToCSV.php</b> in line <b>56</b></div><pre style="display: block; margin: 0; padding: 10px; background: #fff; border: 1px solid #ccc; max-height: 400px; overflow: scroll; font-size: 13px; color: #000;">end</pre></div>
<form action="#" method="POST" enctype="multipart/form-data">
    Название
    <input type="text" name="name" maxlength="255" value="">
    <input type="submit" name="key" value="Выгрузить">
</form>

                 
																															</div>
						</div>
					</section>
				</div>
					</div>		<footer id="footer" >
			<div class="footer_inner">
				<div class="wrapper_inner">
					<div class="footer_top">
						<div class="wrap_md">
							<div class="iblock sblock">
								<!--'start_frame_cache_IzufVt'--><div class="subscribe-form_footer"  id="subscribe-form_footer">
	<div class="wrap_md">
		<div class="wrap_bg iblock">
			<div class="wrap_text">
				<div class="wrap_icon iblock">
					
				</div>
				<div class="wrap_more_text iblock">
					Подписывайтесь<br/> на новости и акции				</div>
			</div>
		</div>
		<div class="forms iblock subscription-form">
				<form action="/personal/subscribe/" class="sform_footer box-sizing">
											<label class="hidden">
							<input type="checkbox" name="sf_RUB_ID[]" value="1" checked /> Новости магазина						</label>
										<div class="wrap_md">
						<div class="email_wrap form-control iblock">
							<input type="email" name="sf_EMAIL" class="grey medium" required size="20" value="" placeholder="Оставьте свой e-mail" />
						</div>
						<div class="button_wrap iblock">
							<input type="submit" name="OK" class="button medium" value="Подписаться" />
						</div>
					</div>

					<div class="clearboth"></div>
				</form>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$("form.sform_footer").validate({
			rules:{ "sf_EMAIL": {email: true} }
		});
	})
</script>
<!--'end_frame_cache_IzufVt'-->							</div>
							<div class="iblock phones">
								<div class="wrap_md">
									<div class="empty_block iblock"></div>
									<div class="phone_block iblock">
										<span class="phone_wrap">
											<span class="icons"></span>
											<span><a rel="nofollow" href="tel:+7–913–746–55–16">+7–913–746–55–16</a></span>
										</span>
										<span class="order_wrap_btn">
											<span class="callback_btn">Заказать звонок</span>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="footer_bottom">
						<div class="wrap_md">
							<div class="iblock menu_block">
								<div class="wrap_md">
									<div class="iblock copy_block">
										<div class="copyright">
											2020 &copy; Хризолит										</div>
										<span class="pay_system_icons">
											<i alt="MasterCard" title="MasterCard" class="mastercard"></i>
<i alt="Visa" title="Visa" class="visa" class="visa"></i>
<i alt="Яндекс.Деньги" title="Яндекс.Деньги" class="yandex_money"></i>
<i alt="WebMoney" title="WebMoney" class="webmoney"></i>
<i alt="Qiwi" title="Qiwi" class="qiwi"></i>										</span>
									</div>
									<div class="iblock all_menu_block">
										<div class="wrap_md submenu_top">
				<div class="menu_item iblock"><a href="/company/">Компания</a></div>
			<div class="menu_item iblock"><a href="/help/">Информация</a></div>
		</div>										<div class="wrap_md">
											<div class="iblock submenu_block">
												<ul class="submenu">
				<li class="menu_item"><a href="/company/">О компании</a></li>
					<li class="menu_item"><a href="/company/news/">По знакам зодиака</a></li>
					<li class="menu_item"><a href="/contacts/stores/">Магазины</a></li>
					<li class="menu_item"><a href="/privacy-policy/">Политика конфиденциальности</a></li>
				</ul>
<script>
	$(".bottom_submenu ul.submenu li").click(function()
	{
		$(".bottom_submenu ul.submenu li").removeClass("selected");
		$(this).addClass("selected");
	});
</script>											</div>
											<div class="iblock submenu_block">
												<ul class="submenu">
				<li class="menu_item"><a href="/help/payment/">Оплата и доставка</a></li>
					<li class="menu_item"><a href="/help/delivery/">Гарантии</a></li>
					<li class="menu_item"><a href="/help/warranty/">Обмен</a></li>
				</ul>
<script>
	$(".bottom_submenu ul.submenu li").click(function()
	{
		$(".bottom_submenu ul.submenu li").removeClass("selected");
		$(this).addClass("selected");
	});
</script>											</div>
											<div class="iblock submenu_block">
												<ul class="submenu">
	</ul>
<script>
	$(".bottom_submenu ul.submenu li").click(function()
	{
		$(".bottom_submenu ul.submenu li").removeClass("selected");
		$(this).addClass("selected");
	});
</script>											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="iblock social_block">
								<div class="wrap_md">
									<div class="empty_block iblock"></div>
									<div class="social_wrapper iblock">
										<div class="social">
												<a href="https://vk.com/club36204008" target="_blank" >
		<img src="/bitrix/components/aspro/social.info.mshop/images/vk.png" alt="ВКонтакте" title="ВКонтакте" />
	</a>
	<a href="https://www.instagram.com/xrizolit.jeweler/" target="_blank" >
		<img src="/bitrix/components/aspro/social.info.mshop/images/inst.png" alt="Instagram" title="Instagram" />
	</a>
 
<div class="company_prog_info">
	<span class="company_prog_text">Разработка и продвижение сайта</span>
	<a href="http://vogood.ru" class="company_prog_link" target="_blank">
		<img src="http://vogood.ru/local/assets/share/vo-logo_dark.svg" class="company_prog_img">
	</a>
</div>										</div>
									</div>
								</div>
								<div id="bx-composite-banner"></div>
							</div>
						</div>
					</div>
														</div>					
			</div>	
		</footer>		
				<div id="content_new"></div>
			
</body>
</html>